## Annex: Treball amb git (comandes)

- Què és git? - https://www.freecodecamp.org/news/what-is-git-learn-git-version-control/

- **Git** és un sistema de "control de versions" dissenyat per Linus Torvalds. Pensant en l'eficiència i confiabilitat de manteniment de versions d'aplicacions amb una enorme quantitat de fitxers de codi font. Permet seguir els canvis fets en el desenvolupament de programari.
- S'ha d'instal·lar com hem vist a l'equip local, i aquest programari es connecta via xarxa a servidors de repositoris que poden ser públics (per exemple **Gitlab** o **Github**) o privats (servidor propi de l'empresa).
- Nosaltres el farem servir per a poder treballar en grup en la documentació dels projectes, ja que pot fer servir repositoris públics com **Gitlab** (que usarem) i **Github** (actualment propietat de Microsoft). Podeu provar i afegir-vos al pla per estudiants que ofereixen) - https://education.github.com/pack

## Passes per usar git

- Git és independent de l'editor que fem servir i te les seves pròpies comandes. A Atom ens ho posen més fàcil per que podem gestionar canvis i fer les operacions en mode gràfic.

- Les comandes més bàsiques són:

1. Per inicialitzar git, cal informar del nom global i el correu global.

  ~~~
  git config --global user.name "Pau T"
  git config --global user.email pau.tome@iescarlesvallbona.cat
  ~~~

1. Canviarem al directori on volem els projectes git, per exemple "cd /home/usuari/git"

1. Per clonar un projecte, si ho fem per HTTPS, Demanarà usuari i password,
  ~~~
  git clone https://gitlab.com/pautome/apunts.git
  ~~~

1. Podem clonar amb autenticació per ssh (cal prèviament instal·lar el certificat generat a Gitlab). Aquesta validació la fareu servir en altres mòduls.
  ~~~
  git clone git@gitlab.com:vallsecurity/m16-priv.git
  ~~~

1. Per afegir un nou element al repositori, per exemple un directori amb codi fem **"add"** (a Atom l'equivalent li diu **"stage All"**). Després fem **"commit"** per validar els canvis. Al final hem de fer **"push"** per pujar els canvis al repositori. Totes les operacions es fan al directori del projecte.

  ~~~
  git add UF1/2-seg-fisica/
  git commit
  git push
  ~~~

  * (també podem fer "git add . " per afegir tot el que hi hagi nou al directori actual. O bé posar la llista a mà dels fitxers que han canviat).

1. Per comprovar canvis i baixar-los executem:

  ~~~
  git fetch
  git pull
  ~~~

---

## En cas de fallada

- _fatal: The remote end hung up unexpectedly_
  ~~~
  git config --global http.postBuffer 1048576000
  ~~~

- _remote: HTTP Basic: Access denied_
  - Pot ajudar fer a una consola:
  ~~~
  git config --system --unset credential.helper
  ~~~
  - Llavors posar bé la contrasenya
  - Alguns alumnes tenen problemes per que han clonat fent "sudo git clone". **Si us plau, NO feu servir sudo per clonar**.
  - En cas que no funcioni, cal anar al projecte Gitlab a la web:
    - En GitLab, Settings > Access Tokens
    - Crea un nou token (marca el checkbox api)
    - Fes el git clone ...
    - Quan demani password, posa el token d'accès en lloc del del teu compte de GitLab.


## Ampliacions Markdown a Gitlab

### Differences between GitLab Flavored Markdown and standard Markdown

GitLab uses standard CommonMark formatting. However, GitLab Flavored Markdown extends standard Markdown with features made specifically for GitLab.

https://docs.gitlab.com/ee/user/markdown.html

### Features not found in standard Markdown:

    - Color chips written in HEX, RGB or HSL
    - Diagrams and flowcharts
    - Emoji
    - Front matter
    - Inline diffs
    - Math equations and symbols written in LaTeX
    - Task Lists
    - Table of Contents
    - Wiki specific Markdown

### TOC

~~~
This sentence introduces my wiki page.

[[_TOC_]]

## My first heading

First section content.

## My second heading

Second section content.
~~~

### Diagrames

You can generate diagrams from text by using: https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts

- Mermaid

Editr: https://mermaid-js.github.io/mermaid-live-editor/

~~~
```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
~~~
Amb subgraphs
~~~
```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```
~~~

- PlantUML


- Kroki to create a wide variety of diagrams.
