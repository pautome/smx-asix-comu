# Problemes amb Atom

## Si desapareix la barra de menú principal (File, Edit, etc).
* Es pot veure picant la tecla "ALT".
* Si volem que la barra d'eines es vegi sempre, podem anar a "View->Toggle menu bar" o bé:
  - A la carpeta personal, entrar al directori .atom
  - Obrir el fitxer config.cson
  - Editar el paràmetre "AutoHideMenuBar: true" i canviar per "AutoHideMenuBar: false".

## Si desapareix la toolbar de Markdown (per a qualsevol plugin d'Atom es pot tocar la seva configuració).
* Anar a Edit, Preferences, Packages.
* Buscar tool-bar.
* Picar botó settings.
* Marcar checkbox "visible".
* També es pot fer: Anar a view, "toggle toolbar".

## Si desapareix la finestra a l'esquerra amb l'arbre de fitxers.
* Anar a "View -> toggle tree view".

## Per configurar qualsevol paquet de l'Atom:
* aneu a Edit, Preferences, Packages.
* Escolliu el paquet i busqueu Settings.
* També podreu accedir a l'ajuda del paquet (que estarà penjada a Internet).

## Les imatges no es veuen
* Assegureu-vos bé de que els noms de fitxers i directoris no inclouen espais, lletres amb accent, ñ, ni cap símbol que no sigui anglosaxó.
* Assegureu-vos que la ruta a les imatges és correcta i que és relativa al directori actual (no ha de començar MAI amb / ja que seria una ruta ABSOLUTA).
* Assegureu-vos que el nom del fitxer té majúscules i minúscules exactament igual. En Linux no és el mateix "imatge.png" que "imatge.PNG". Això sol passar si feu la pràctica en Windows (que posa l'extensió en majúscules al Markdown però no al propi fitxer) i després es visualitza en Linux (llavors no ho troba).
