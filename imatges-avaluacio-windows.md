
## Imatges Windows per les pràctiques

- Màquines virtuals preinstal·lades win7, win81, win10 - https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/

- En Azure, disc ISO, Disc dur virtual VHD de Windows Server 2012 (descàrrega directa)
  - https://go.microsoft.com/fwlink/p/?linkid=2195172&clcid=0x409&culture=en-us&country=us
  - https://www.microsoft.com/es-es/evalcenter/evaluate-windows-server-2012-r2

- Versions d'avaluació de Microsoft - https://www.microsoft.com/es-es/evalcenter (obrir menu de dalt)
