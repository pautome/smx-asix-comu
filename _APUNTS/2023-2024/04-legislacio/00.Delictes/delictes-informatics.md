# DELICTES INFORMATICS

**INS Carles Vallbona**

**Pau Tomé**

(Act Oct-2023)

<p align="center">
<img src="imgs/delictes-informatics-5da0be60.png" width="600" />
</p>

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [DELICTES INFORMATICS](#delictes-informatics)
	- [QUE SON ELS DELICTES?](#que-son-els-delictes)
		- [DELICTE CIVIL](#delicte-civil)
			- [PENES PER DELICTE CIVIL](#penes-per-delicte-civil)
		- [DELICTE PENAL](#delicte-penal)
			- [PENES PER DELICTE PENAL](#penes-per-delicte-penal)
	- [QUE SON ELS DELICTES INFORMATICS?](#que-son-els-delictes-informatics)
		- [DELICTE D'INTRUSIO INFORMÁTICA (ART 197BIS)](#delicte-dintrusio-informtica-art-197bis)
		- [INTERCEPTACIO DE TRANSMISSIONS DE DADES INFORMATIQUES (ART 197 BIS AP.SEGON)](#interceptacio-de-transmissions-de-dades-informatiques-art-197-bis-apsegon)
	- [ALTRES DELICTES INFORMATICS](#altres-delictes-informatics)
		- [AMENACES (ART 169)](#amenaces-art-169)
		- [DELICTES CONTRA L'HONOR: CALUMNIES I INJURIES (ART 205 I SS)](#delictes-contra-lhonor-calumnies-i-injuries-art-205-i-ss)
		- [FRAUS INFORMÁTICS (ESTAFA) (ART 248)](#fraus-informtics-estafa-art-248)
		- [SABOTATGE INFORMATIC (ART 264)](#sabotatge-informatic-art-264)
		- [PORNOGRAFIA INFANTIL (ART 189)](#pornografia-infantil-art-189)
		- [FALSEDAT (ART 390-SS)](#falsedat-art-390-ss)
	- [REFERENCIES](#referencies)

<!-- /TOC -->
---

## QUE SON ELS DELICTES?

Un delicte és una **conducta típica, antijurídica, culpable i per tant subjecte a càstig**.

Els delictes es poden classificar en dos grups
* Delicte civil
* Delicte penal

### DELICTE CIVIL

Un delicte civil és un acte il·lícit amb **intenció de danyar als altres**. Per exemple:

* Una persona deixa de pagar el lloguer i se li reclama judicialment el pagament i el desnonament.
* Una persona trenca un objecte d'una altra, i aquesta li reclama el pagament de l'objecte.
* Un metge opera malament a una pacient i aquesta  decideix demandar-lo per mala praxis.
* Es produeix un xoc i una de les parts demanda a l'altra per danys i perjudicis.
* En l'exemple anterior, qui va haver de pagar danys i perjudicis demanda a l'asseguradora pel reintegrament del que va pagar.

#### PENES PER DELICTES CIVILS

* Les condemnes contemplen la restitució del dany i les indemnitzacions econòmiques.
* També els danys morals.

---

### DELICTE PENAL

Un delicte penal és el que està **tipificat en el Codi Penal**
* Si no hi és al codi, no és delicte penal.
* Exemples:
  * Intent d’homicidi.
  * Causar lesions.

#### PENES PER DELICTE PENAL

Les **penes pels delictes penals** poden ser de presó i multes. Una multa és una quota de 2 a 400 euros diaris, durant el temps que s'indiqui. https://www.iberley.es/temas/pena-multa-tipos-47201

* És habitual que es posin penes que incloguin les dues coses.
* A vegades un delicte civil pot també ser penal i a l’inrevés.

**Es poden cometre diversos delictes alhora en un sol acte**... Per exemple entrant remotament en un equip aliè per obtenir informació de targetes de crèdit. Llavors s'han comés els delictes:
  * Robar diners
  * Accés il·legal
  * Pirateria
  * Descobriment de secrets

I a més hi ha una condemna diferent per cadascun.

---

## QUE SON ELS DELICTES INFORMATICS?

En la legislació espanyola no existien els delictes informàtics. Es considerava simplement que eren delictes fets amb un sistema informàtic.

`“.. qualsevol acte il·lícit penal dut a terme a través de mitjans informàtics i que està íntimament lligat als bens jurídics relacionats amb les tecnologies de la informació o que té com a fi aquests bens”
`

Però a partir de la reforma del codi penal de **Març de 2015** es contemplen ja delictes informàtics com a tals.

Aquests delictes han crescut tant que les policies han creat cossos especialitzats
* **GDT - Grupo de Delitos Telemáticos** - Guàrdia civil
* **BCIT - Brigada Central de Investigación Tecnológica** - Policía Nacional - https://www.policia.es/org_central/judicial/udef/bit_quienes_somos.html
* **UCDI - Unitat Central de Delictes Informàtics** - Mossos d'esquadra
*  **Unitat d'Informàtica Forense (de la Divisió de Policia Científica)** - Mossos d'esquadra

<p align="center">
<img src="imgs/delictes-informatics-ce8fcc3f.png" width="200" />
<img src="imgs/delictes-informatics-5fdde3cd.png" width="200" />
<img src="imgs/delictes-informatics-389b95d6.png" width="200" />
</p>

El Conveni sobre cibercriminalitat o **Conveni de Budapest** (23/11/2001) va definir la base per:
* Harmonitzar les lleis dels diferents països
* Facilitar la cooperació internacional

El conveni definia grups de delictes informàtics:

1. Delictes contra la **confidencialitat, la integritat i la disponibilitat**.
2. Delictes informàtics (falsificació, frau).
3. Delictes relacionats amb el contingut (pornografia infantil).
4. Infraccions de la propietat intel·lectual.

La policia espanyola en considera algun més (del codi penal)

5. Delictes contra l’honor
6. Amenaces i coaccions
7. Delictes contra la salut pública

---

### DELICTE D'INTRUSIO INFORMÁTICA (ART 197BIS)

Aquest delicte castiga **l'accès o la facilitació de l'accès al conjunt o part d'un sistema d'informació**, vulnerant mesures de seguretat i sense autorització.
* Es castiga fins i tot si no s'ha accedit a dades, amb penes de **presó de sis mesos a dos anys**.
* Exemple: Eines de reconeixement com ara **nmap** i d'atac a contrasenyes online com ara **Hydra**.

---

### INTERCEPCIÓ DE TRANSMISSIONS DE DADES INFORMATIQUES (ART 197 BIS AP.SEGON)

**Interceptar transmissions no públiques** de dades informàtiques, inclòs via senyal sense fils.
* Es castiga amb **presó de 3 mesos a dos anys o multa de 3 mesos a 2 anys**.

* Exemple: Usar sniffers en cable i wifi, keyloggers, troians, ...

---

## ALTRES DELICTES INFORMATICS

### AMENACES (ART 169)

Es castiguen les amenaces realitzades o difoses per qualsevol mitjà  de comunicació.
- **Penes de presó de 6 mesos a 5 anys segons gravetat.**


### DELICTES CONTRA L'HONOR: CALUMNIES I INJURIES (ART 205 I SS)

**CALUMNIA**: Una calumnia és acusar a algú d’haver comès un delicte sabent que és fals
- **Penes de presó 6 mesos a 2 anys o multa de 12 a 24 mesos**

**INJURIA**: Una injúria consisteix en lesionar la dignitat o l’honor d’una persona
- **Penes de multa de 6 mesos a 14 mesos o multa de 12 a 24 mesos**

<p align="center">
<img src="imgs/delictes-informatics-739c08a2.png" width="400" />
</p>

Font: https://es.wikipedia.org/wiki/Injurias_a_la_Corona_(Espa%C3%B1a)#Revista_El_Jueves

---

### FRAUS INFORMÁTICS (ESTAFA) (ART 248)

Es considera un **frau** quan amb ànim de lucre s'enganyi a algú perquè faci alguna cosa que el perjudiqui.
* El més corrent és perjudicar-lo **econòmicament**.
* **Penes de presó de sis mesos a 3 anys**
* Exemple: el phising bancari.

---

### SABOTATGE INFORMATIC (ART 264)

Es tracta de:
* Esborrar o danyar dades o programes.
* Interrompre o entorpir el funcionament d'un sistema informàtic, el que coneixem com a DoS o DDoS.
* Crear, facilitar, posseir programes o contrasenyes o codis d'accés per cometre els delictes anteriors.

- **Penes de presó 6 mesos a 3 anys**.

---

### PORNOGRAFIA INFANTIL (ART 189)

Producció, venda, distribució, exhibició o possessió de material pornogràfic de menors.
* **Penes de 5 a 9 anys de presó**.

---

### FALSEDAT (ART 390-SS)

Alterar alguna cosa per fer-la passar per autèntica és un delicte de falsificació
* També es castiga la possessió de software informàtic per cometre delictes de falsedat (ART 400).

- **Penes de 4 a 8 anys de presó per falsificar targetes de crèdit**.  

---

## REFERENCIES

* Delictes informàtics al codi penal: http://www.legaltoday.com/practica-juridica/penal/penal/los-nuevos-delitos-informaticos-tras-la-reforma-del-codigo-penal#
* GDT Guàrdia Civil: https://www.gdt.guardiacivil.es/webgdt/pinformar.php
* BCIT Policia nacional:
  - https://ca.wikipedia.org/wiki/Unitat_Central_de_Delictes_Inform%C3%A0tics
  - https://www.policia.es/org_central/judicial/udef/bit_quienes_somos.html
* UCDI Mossos - https://ca.wikipedia.org/wiki/Unitat_Central_de_Delictes_Inform%C3%A0tics
* Investigació criminal Mossos (amb policia científica, informàtica forense)- https://ca.wikipedia.org/wiki/Comissaria_General_d%27Investigaci%C3%B3_Criminal
