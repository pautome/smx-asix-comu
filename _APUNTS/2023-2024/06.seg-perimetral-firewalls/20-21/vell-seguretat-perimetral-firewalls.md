# Firewalls i seguretat perimetral
**INS Carles Vallbona**
**Pau Tomé**

# Mesures de seguretat perimètrica

- Tallafocs (organitzant DMZ’s)
- IDS (Intrusion Detection System) i IDPS (ID and Prevention System)
- Passarel·les antivirus i antispam
- Honeypots

https://sergvergara.wordpress.com/2011/03/14/arquitectura-y-diseno-de-seguridad-de-red-perimetral/

# Firewall

- Dispositiu o conjunt de dispositius configurats per blocar l’accés no autoritzat, permetent comunicacions autoritzades.
- Permeten, limiten, xifren, desxifren, el tràfic entre els diferents àmbits (INTERN o LAN - EXTERN o WAN - DMZ) sobre la base d’un conjunt de normes i d’altres criteris.

![Tallafocs](imgs/seguretat-perimetral-firewalls-5269123d.png)

### Regla d’or

- A part dels tallafocs sempre s'ha de tenir present una de les regles bàsiques de la
seguretat:
    - Només tenir engegats els serveis estrictament necessaris.
    - La idea és minimitzar l'exposició tant com sigui possible.

# Tipus de Firewall

- Es poden categoritzar segons determinats criteris:

1. **Implementació**:
    - Maquinari
    - Programari.


1. **Ubicació**:
    - **Corporatius o de xarxa**: Entre xarxes LAN i WAN o entre Intranets. Protegeixen el pas entre xarxes.
    - **Basats en host (personal)**: Protegeixen un sol host.


1. **Comportament i nivell del model OSI** on s’apliquen:
    - **Firewall de paquets (packet filter)**.
    - **Firewalls amb control d’estat (stateful firewall)**.
    - **Firewalls d’aplicació (Application Layer o Proxy)**.

# Generacions de firewall:

- Al llarg de la història s’han desenvolupat diferents tipus de Firewalls (evolució):

    - **Firewall de paquets (packet filter)**.
    - **Firewalls amb control d’estat (stateful firewall)**.
    - **Firewalls d’aplicació (Application Layer o Proxy)**.

# Firewalls de paquets (packet filter)

- Treballen als nivells del model OSI:
    - 3 (Xarxa)
    - 4 (Transport).

- En el cas de TCP/IP, treballen sobre els nivells:
    - **IP (Capa 3 o Internet)**: Analitzen la capçalera IP per aplicar les restriccions (per exemple, adreces IP d’origen i destí)
    - **TCP/UDP (Capa 4 o de Transport)**: Analitzen la capçalera TCP o UDP per aplicar les restriccions (per exemple ports d’origen i destí).

![](imgs/seguretat-perimetral-firewalls-16bd7db6.png)
Pila TCP/IP

![](imgs/seguretat-perimetral-firewalls-bfa3c93b.png)
Capçalera de paquet IP

![](imgs/seguretat-perimetral-firewalls-c892ea77.png)
Capçalera de segment TCP

# Firewalls amb control d’estat (stateful)

- Segona generació de tallafocs, millora l’anterior.
- Tenen en compte, a més, **la col·locació de cada paquet individual dintre d’una sèrie de paquets**.
- Es coneix generalment com la “**inspecció d’estat de paquets**”, ja que manté registres de totes les connexions que passen pel tallafocs.
- Pot determinar si un paquet indica:
    - l’inici d’una nova connexió
    - és part d’una connexió existent
    - és un paquet erroni.


- Poden ajudar a **prevenir atacs contra connexions en curs o certs atacs de denegació de servei**.

# Firewalls de nivell d’aplicació (application layer)

- Actuen sobre la **capa d’aplicació del model OSI**.
- Pot **entendre certes aplicacions i protocols** (ex: protocol de transferència de fitxers o FTP, DNS o navegació web)
- Permet **detectar si un protocol no desitjat està funcionant per un port no estàndard o si s’abusa d’un protocol de forma perjudicial**.
- Molt **més segur i fiable** comparat amb tallafocs de filtrat de paquets, ja que repercuteix sobre les 7 capes del model OSI.
- Similar a un tallafocs de filtrat de paquets, però també **permet filtrar el contingut del paquet**.

- Exemples: ISA (Internet Security and Acceleration) de Microsoft o Squid Proxy.
- Exemple d’ús: Es pot blocar tota la informació relacionada amb una paraula clau, blocar per capçaleres HTTP, mida d’arxius, etc.

- **Inconvenient**: Són més lents que els firewall d’estat.

- Poden **filtrar protocols de capes superiors** com:
  - FTP
  - TELNET
  - DNS
  - DHCP
  - HTTP
  - TCP, UDP
  - TFTP, etc.

# Limitacions dels tallafocs (De què no pot protegir)

- Contra atacs que facin servir **tràfic que no passi per ell**.
- D’amenaces per **atacs interns** o d’usuaris negligents.
- Que espies corporatius **copiïn dades sensibles a mitjans físics d’emmagatzemament** i robar-les.
- Contra atacs d’**enginyeria social**.
- Contra atacs possibles a la xarxa interna per **malware arribat via fitxers** (tot i que pot filtrar si passen per ell).
- De **fallades de seguretat de serveis i protocols del que hem permès el tràfic**. Cal configurar correctament i cuidar la seguretat dels serveis que es publiquin a Internet.

# Configuració de tallafocs

- Existeixen dos tipus de polítiques de configuració:
    - **Permissiva**: Permetre tot el tràfic (ip’s, ports, protocols, etc) i blocar només el que no es vol.
    - **Restrictiva**: No permetre cap tràfic de xarxa i aplicar excepcions al tràfic que compleix les condicions.


- Sempre és **més segur el model Restrictiu**.

# Configuració de tallafocs

- En general un tallafocs té **tres formes d'actuar** davant d'un intent de connexió:
  1. **Permetre (ACCEPT)**: Deixem passar la connexió en direcció al destí al que anava dirigit
  2. **Denegar (DENY)**: Denega la connexió i envia un missatge al que la iniciava
  3. **Ignorar (DROP)**: Ignora la petició descartant el paquet

# Seguretat perimètrica: Arquitectura 1 router

![Xarxa local o intranet darrera un router: tot el tràfic permès](imgs/seguretat-perimetral-firewalls-04cbdd01.png)

# Seguretat perimètrica: Arquitectura 1 router

- Usat en petites xarxes com les domèstiques i ho sol fer el router ADSL o de fibra.
- Un sol router que no sol filtrar les peticions que s’envien des de la xarxa interna.
- Funciona amb redirecció de ports per a reenviar peticions externes cap a servidors de la xarxa interna.

# Seguretat perimètrica: Arquitectura 1 router

- Xarxa plana sense segmentar.
- Publicació de serveis interns.
- No hi ha elements de monitorització.
- No es filtra tràfic d’entrada ni sortida.
- No es verifica malware o spam en el correu electrònic.
- Els clients remots accedeixen directament als serveis.

# Seguretat perimètrica: Arquitectures amb DMZ

- **DMZ: Zona desmilitaritzada**. Zona insegura ubicada entre la xarxa interna i una xarxa externa, normalment Internet.
- Objectiu: Les connexions entre la xarxa Externa i la DMZ estan permeses, però no entre l’externa i la interna o la DMZ i la interna.
- Els equips de la xarxa DMZ donen serveis a l’externa (servidors públics) i es protegeix la xarxa interna en cas que els servidors siguin compromesos.

# DMZ de host

![](imgs/seguretat-perimetral-firewalls-3a2d222f.png)

- Router/Firewall reenvia peticions externes a un sol host (Bastió). Verd tràfic permés, vermell tràfic prohibit.

# DMZ de host

- Alguns routers ho permeten i es tracta de reenviar tot el tràfic extern cap a un sol host.
- Es pot configurar el host bastió com a firewall proxy i obligar els hosts de la xarxa a passar per ell via configuració.
- El host bastió pot accedir a la xarxa interna i per tant no és tant segur com una DMZ amb 3 potes o amb 2 firewalls.

# Dual homed host (o gateway)

- Tot el tràfic entre xarxa interna i externa prohibit (vermell). Tot passa pel bastió, que filtra el que no es vol.

![Dual homed host](imgs/seguretat-perimetral-firewalls-8c220d3e.png)

# Dual homed host (o gateway)

![Bastió](imgs/seguretat-perimetral-firewalls-2fb37403.png)

- **BASTIÓ**:

(1) reducte fortificat que es projecta cap a l'exterior del cos principal d'una fortalesa, situat generalment en els cantons dels murs (anomenats murs de cortina), com punt fort de la defensa contra l'assalt de tropes enemigues.
(2) Aplicació que sol estar en un servidor amb el fi d’oferir seguretat a la xarxa interna. És configurat especialment per la recepción d’atacs

# Dual homed host (o gateway)

- No hi ha cap tipus de reenviament (forwarding)
- Només es permet connectar amb l'exterior al Bastió que fa de proxy.
- No es permeten connexions directes interna-externa.
- S'amaga la xarxa interna dels atacants

# DMZ amb un sol firewall (3 legged model)

![3 legged model](imgs/seguretat-perimetral-firewalls-78aeab00.png)

# DMZ amb un sol firewall (3 legged model)

- Un sol firewall amb al menys 3 interfícies de xarxa (externa, DMZ, interna).
- El firewall es converteix en un punt únic de fallida i ha de gestionar tot el tràfic per la DMZ i la xarxa interna.
- No es permet tràfic directe entre externa i interna.

# DMZ amb dos firewalls

![DMZ amb 2 firewalls](imgs/seguretat-perimetral-firewalls-56e57e92.png)

# DMZ amb dos firewalls

- Configuració **més segura**, ja que implica que un atac ha de controlar dos firewalls.
- Seria interessant que fossin de **diferents marques**.
- El primer firewall (perimetral) es configura per a permetre l’accés a la DMZ.
- El segon firewall (intern) es configura per a permetre el tràfic de la DMZ a la xarxa interna.

# Altres mesures de seguretat perimetral: IDS i IDPS

- **IDS: Intrusion Detection System**. Dispositius que monitoritzen i generen alarmes si es produeixen alertes de seguretat.
- **IDPS: Intrusion Detection and Prevention System**. A més de l’anterior, bloquen l’atac evitant que tingui efecte.
-
Informació sobre IDS i IDPS:
https://www.rediris.es/cert/doc/unixsec/node26.html


# IDS i IDPS

- **Funcions**:
    - Identificació dels atacs (en mode passiu o IDS).
    - Registre d’events (fitxers de log).
    - Bloqueig dels atacs (en mode actiu, en el cas de IDPS).
    - Informar els administradors de les anomalies (per correu-e, etc.)

- **Tipus de IDS**
    - **HIDS: Host IDS**, monitoritza canvis en el sistema operatiu i les aplicacions. Funciona a nivell d’equip local.
    - **NIDS: Network IDS**, monitoritza el tràfic de la xarxa.

# IDS vs Firewalls

- Els Firewalls examinen el tràfic extern per blocar tràfic atacant, **no estan dissenyats per avisar**. Tampoc detecten atacs provinents de la xarxa interna.
- Els IDS observen el tràfic de la xarxa i **detecten i avisen dels atacs**, inclús si aquests provenen de la xarxa interna. Els IDPS tanquen les connexions considerades atacs.

# Mètodes de detecció d’intrusions: firmes i heurística

- **Firmes**: S’analitzen els paquets de la xarxa i es comparen amb patrons d’atac coneguts i preconfigurats (firmes).
- Cal preconfigurar les firmes i només es detecten atacs coneguts, mai els nous.
- Per exemple, miren seqüències de bytes determinades en el tràfic de xarxa, o una seqüència determinada d’instruccions usades per malware.

# Mètodes de detecció d’intrusions: firmes i heurística

- **Heurística**: Es determina l’actividad normal de xarxa (com l’ample de banda usat, protocols, ports i dispositius que generalmente s’interconnecten) i alerta a un administrador quan aquest varia del normal.
- Permet detectar atacs desconeguts, però pot generar falsos positius.
- Exemple: Emmagatzemar quins equips accedeixen als servidors de les BD de nòmines i alertar quan algú altre hi accedeix.

# Ubicació dels IDS

La ubicació dels IDS és molt important:

- Darrera el firewall: Només detectaria atacs des de l’exterior.
- Connectat a un port d’inspecció del switch (mirror port):
      - **Tots els ports**: Veuria tot el tràfic, però cal anar en compte per que pot generar molt de tràfic a l’IDS. També es poden generar moltes alertes inexistents.
      - **Només ports de servidors**: Vigilem els atacs a servidors.

![Port mirror](imgs/seguretat-perimetral-firewalls-57f4e8b9.png)

# Ubicacions dels IDS
![Possibles ubicacions dels IDS](imgs/seguretat-perimetral-firewalls-e7e411cc.png)

# IDS vs IDPS

![IDS vs IDPS](imgs/seguretat-perimetral-firewalls-bc74746a.png)


# Limitacions dels IDS

- **Soroll**: Els afecta molt el soroll (paquets generats per errors de programari, per exemple).
- **Falses Alarmes**: Hi ha moltes més falses alarmes que atacs reals, per tant poden fer que s’ignorin alarmes d’atacs reals!!
- Les **firmes obsoletes** fan que no es detectin noves formes d’atacs.
- Hi ha un **retard entre el descobriment d’un tipus d’atac i la creació de la firma** corresponent.


# Limitacions dels IDS

- No poden evitar atacs fets contra autenticació dèbil.
- Molts no poden treballar amb paquets encriptats.
- Es refien de les adreces IP dels paquets, però molts atacs les falsifiquen (IP spoofing).
- Poden ser vulnerables als mateixos atacs orientats a protocols que els hosts de la xarxa i fer-los caure.

# Exemples de NIDS (de xarxa) open source

https://protegermipc.net/2017/02/22/mejores-ids-opensource-deteccion-de-intrusiones/

![](imgs/seguretat-perimetral-firewalls-235e2625.png)

**SNORT**: Projecte amb solera (1998). Molt de suport de la comunitat.
- Configuració complexa (consola) però disposa d’eines gràfiques com Snorby o Squil.

# Exemples de NIDS (de xarxa) open source

![](imgs/seguretat-perimetral-firewalls-cecc4d24.png)

**SURICATA**: Molt semblant a Snort i amb les mateixes firmes.

- Avantatges: Execució multifil, acceleració hardware amb GPU, extracció de fitxers per exemple de malware, llenguatge d’scripts, anàlisi de certificats TLS/SSL, etc.

# Exemples de NIDS (de xarxa) open source

![](imgs/seguretat-perimetral-firewalls-8ff047f7.png)

**BRO-IDS**: Basat en firmes i anomalies, permet capturar tràfic per a crear el nivell base. Corva d’aprenentatge forta. Permet desconnectar atacants.

# Exemples de NIDS (de xarxa) open source

![](imgs/seguretat-perimetral-firewalls-fd52c30e.png)

**KISMET**: IDS per a xarxes Wireless. Pot detectar AP’s falsos i evitar la suplantació d’AP’s avisant de la seva presència.

# Exemples de HIDS

![](imgs/seguretat-perimetral-firewalls-60ffb464.png)

**OSSEC**: Corre sobre qualsevol sistema operatiu i envia la informació a través de xarxa per evitar perdre-la en cas de rebre un atac.

- Es pot instal·lar un servidor centralitzat per recollir les mostres de tots els agents instal·lats en els hosts.

# Exemples de HIDS

![](imgs/seguretat-perimetral-firewalls-ecefafea.png)

**Tripwire Opensource**: Versió de la comunitat per a portar control d’intrusions i d’integritat dels arxius del sistema. Crea fitxers de hash i els guarda xifrats per anar comprovant si hi ha hagut modificacions no autoritzades.

# Honeypots (Pots de mel)

- Sistemes dissenyats per a protegir-se d’un possible atac i per a servir d’esquer per a un atacant, de manera que es pot detectar l’atac abans que afecti sistemes crítics.

![Honeypot](imgs/seguretat-perimetral-firewalls-7620f901.png)

- Si es tracta d’una xarxa completa, es diuen Honey Net.

- Objectius:
    - Alertar de l’existència d’un atac
    - Obtenir informació sense interferir en l’atac
    - Intentar alentir l’atac i protegir així la resta del sistema.

- Exemples: Detectar malware, spammers, enganyar i desgastar l’atacant, servidors de BD falsos per evitar atac al bo, etc.

# Exemples de honeypots

- **Deception toolkit**: http://www.all.net/dtk/ Es basa en implementar un equip que simula molts serveis amb vulnerabilitats conegudes. L’engany com a contramesura.

- **Kippo**: Honeypot per a simular servidor SSH i detectar atacs de força bruta i la interacció SSH amb l’atacant.

- **Valhala**: Honeypot Windows que simula http (web), ftp, tftp, finger, pop3, smtp, echo, daytime, telnet i port forwarding (alguns reals i altres simulats).

- **KFSensor**: Comercial per a Windows.


---

# Més informació a estudiar

- Seguretat perimetral: https://sergvergara.wordpress.com/2011/03/14/arquitectura-y-diseno-de-seguridad-de-red-perimetral/

- Tallafocs i DMZ: https://www.rediris.es/cert/doc/unixsec/node23.html

- Vídeo seguretat perimetral: http://www.criptored.upm.es/intypedia/video.php?id=introduccion-seguridad-perimetral&lang=es

- Informació sobre IDS i IDPS:
https://www.rediris.es/cert/doc/unixsec/node26.html
http://slideplayer.es/slide/10920509/
http://slideplayer.es/slide/3588428/
