# FIREWALLS I SEGURETAT PERIMETRAL

![](imgs/1.firewall-IP-Tables-207415b9.png)

**INS Carles Vallbona**

**Pau Tomé** (Act nov 2023)

(2023) Conceptes previs:
- IP, mask, gateway, DNS: https://iximiuz.com/en/posts/computer-networking-101/
- Mapes de xarxa lògic. Representar dispositius. Programari per a dibuixar.
- Taula de rutes
- Segmentació - símil: separar un espai obert en diferents cubicles i/o fer aules per a separar visualment i acústicament.
- Protocols de xarxa amb model OSI - Fer exemple de "burocràcia" per entendre les capes.

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [FIREWALLS I SEGURETAT PERIMETRAL](#firewalls-i-seguretat-perimetral)
	- [MESURES DE SEGURETAT PERIMETRICA](#mesures-de-seguretat-perimetrica)
	- [FIREWALL](#firewall)
	- [TIPUS DE FIREWALL](#tipus-de-firewall)
		- [IMPLEMENTACIÓ:](#implementaci)
		- [UBICACIO](#ubicacio)
		- [COMPORTAMENT I NIVELL DEL MODEL OSI ON S’APLIQUEN:](#comportament-i-nivell-del-model-osi-on-sapliquen)
	- [GENERACIONS DE FIREWALL](#generacions-de-firewall)
		- [FIREWALLS DE PAQUETS (PACKET FILTER)](#firewalls-de-paquets-packet-filter)
		- [FIREWALLS AMB CONTROL D’ESTAT (STATEFUL)](#firewalls-amb-control-destat-stateful)
		- [FIREWALLS DE NIVELL D’APLICACIO (APPLICATION LAYER)](#firewalls-de-nivell-daplicacio-application-layer)
	- [LIMITACIONS DELS TALLAFOCS (DE QUE NO POT PROTEGIR)](#limitacions-dels-tallafocs-de-que-no-pot-protegir)
	- [CONFIGURACIO DE TALLAFOCS](#configuracio-de-tallafocs)
	- [ARQUITECTURES DE SEGURETAT AMB FIREWALLS](#arquitectures-de-seguretat-amb-firewalls)
		- [ARQUITECTURA 1 ROUTER](#arquitectura-1-router)
	- [SEGURETAT PERIMÈTRICA: ARQUITECTURES AMB DMZ](#seguretat-perimtrica-arquitectures-amb-dmz)
		- [DMZ DE HOST (BASTIO)](#dmz-de-host-bastio)
		- [DUAL HOMED HOST (O GATEWAY)](#dual-homed-host-o-gateway)
			- [BASTIÓ](#basti)
		- [DMZ AMB UN SOL FIREWALL (3 LEGGED MODEL)](#dmz-amb-un-sol-firewall-3-legged-model)
		- [DMZ AMB DOS FIREWALLS](#dmz-amb-dos-firewalls)
	- [ALTRES MESURES DE SEGURETAT PERIMETRAL: IDS I IDPS](#altres-mesures-de-seguretat-perimetral-ids-i-idps)
		- [IDS i IDPS](#ids-i-idps)
			- [TIPUS DE IDS](#tipus-de-ids)
			- [IDS VS FIREWALLS](#ids-vs-firewalls)
		- [METODES DE DETECCIO D’INTRUSIONS: FIRMES I HEURISTICA](#metodes-de-deteccio-dintrusions-firmes-i-heuristica)
			- [FIRMES](#firmes)
			- [HEURISTICA](#heuristica)
		- [UBICACIO DELS IDS](#ubicacio-dels-ids)
		- [LIMITACIONS DELS IDS](#limitacions-dels-ids)
		- [EXEMPLES DE NIDS (DE XARXA) OPEN SOURCE](#exemples-de-nids-de-xarxa-open-source)
			- [SNORT](#snort)
			- [SURICATA](#suricata)
			- [BRO-IDS](#bro-ids)
			- [KISMET](#kismet)
		- [EXEMPLES DE HIDS](#exemples-de-hids)
			- [OSSEC](#ossec)
			- [Tripwire Opensource](#tripwire-opensource)
	- [Altres mesures de seguretat perimetral: Honeypots (Pots de mel)](#altres-mesures-de-seguretat-perimetral-honeypots-pots-de-mel)
		- [EXEMPLES DE HONEYPOTS](#exemples-de-honeypots)
- [REFERENCIES I MES INFORMACIO A ESTUDIAR](#referencies-i-mes-informacio-a-estudiar)

<!-- /TOC -->
---

## MESURES DE SEGURETAT PERIMETRICA

Vídeo seguretat perimetral: http://www.criptored.upm.es/intypedia/video.php?id=introduccion-seguridad-perimetral&lang=es

Per a protegir la nostra xarxa local de possibles atacs s'organitzen les següents mesures de seguretat:
- Tallafocs o firewalls, organitzant DMZ’s (DeMilitarized Zones)
- IDS (Intrusion Detection System) i IDPS (ID and Prevention System)
- Passarel·les antivirus i antispam
- Honeypots

https://sergvergara.wordpress.com/2011/03/14/arquitectura-y-diseno-de-seguridad-de-red-perimetral/

---

## FIREWALL

El firewall és una de les mesures més importants per protegir la nostra xarxa.

Un Firewall és un dispositiu o conjunt de dispositius configurats per blocar el tràfic de xarxa no autoritzat, alhora que permeten comunicacions autoritzades.

Els firewalls permeten, limiten, xifren, desxifren, el tràfic entre els diferents àmbits, sobre la base d’un conjunt de normes i d’altres criteris. Els àmbits són:
- EXTERN o WAN
- INTERN o LAN
- DMZ (on hi ha serveis de xarxa públics)

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-5269123d.png" width="400" />
</p>

<p align="center">
Tallafocs
</p>

---

A part dels tallafocs sempre s'ha de tenir present **una de les regles bàsiques de la seguretat**:
- Només cal tenir engegats els serveis estrictament necessaris.
- Cal minimitzar l'exposició dels equips de la xarxa tant com sigui possible.

---

## Símil de tallafocs

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-05d881d2.png" width="300" />
</p>

<p align="center">
Una xarxa d'entrenament de futbol és una forma de visualitzar un tallafocs.
</p>

---

## TIPUS DE FIREWALL

Es poden categoritzar segons determinats criteris:

### 1. IMPLEMENTACIÓ:

Poden ser:
1. **Implementats en maquinari**:
  - Molt més cars.
  - Molt més eficients, per a ser maquinari dedicat.
  - Gairebé sempre són propietaris.

2. **Implementats en programari**:
	- Més barat per que es pot fer servir un ordinador.
	- Menys eficients.
	- Amb versions open-source o lliures.     
  - També per l'opció de protegir un sol ordinador.

---

### 2. UBICACIO

1. **Corporatius o de xarxa**:
   - Entre xarxes LAN i WAN o entre Intranets.
   - Protegeixen el pas entre xarxes.

2. **Basats en host (personal)**:
   - Protegeixen un sol host.
   - Programari de firewall, normalment forma part del sistema operatiu.

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-035aa820.png" width="400" />
</p>

---

Un símil per entendre millor la idea és un guarda de seguretat a la porta d'un establiment que vigila qui passa i decideix si entra o no entra.

---

### 3. COMPORTAMENT I NIVELL DEL MODEL OSI ON S’APLIQUEN:

Al llarg de la història s’han desenvolupat diferents tipus de Firewalls, en 3 generacions:
- **1. Firewall de paquets (packet filter)**, al nivell 3, XARXA i nivell 4, TRANSPORT.
- **2. Firewalls amb control d’estat (stateful firewall)**, al nivell 3, XARXA i nivell 4, TRANSPORT. Afegeixen memòria per recordar connexions.
- **3. Firewalls d’aplicació (Application Layer o Proxy)**, al nivell 7, APLICACIÓ.

---

#### 3.1. FIREWALLS DE PAQUETS (PACKET FILTER)

Són els firewalls que veurem, els més bàsics. Aquests firewalls treballen als nivells del model OSI i TCP/IP 3 i 4.

En el cas de TCP/IP, treballen sobre els nivells:
- **IP (Capa 3 o Internet)**: Analitzen la capçalera IP per aplicar les restriccions (per exemple, adreces IP d’origen i destí)
- **TCP/UDP (Capa 4 o de Transport)**: Analitzen la capçalera TCP o UDP per aplicar les restriccions (per exemple ports d’origen i destí).

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-16bd7db6.png" width="600" />
</p>


<p align="center">
Pila TCP/IP: Treballen a nivell 3 i 4
</p>

---

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-03a6723b.png" width="600" />
</p>

<p align="center">
Capçalera TCP/IP d'un paquet. En verd camps que es comproven al firewall de paquets
</p>

---

Un **símil per entendre millor la idea de firewall de paquets** és el control de la policia a l'entrada de l'aeroport. El policia-firewall et pregunta:
- D'on vens (adreça IP origen)?
- on vas (adreça IP destí)?
- Què vas a fer (port TCP/UDP destí)?

El port de destí d'un paquet determina el servei que es vol utilitzar (**sempre i quan es respecti l'estàndard**). Exemples:
  - SSH port 22
  - Web port 80 i 443 per a web segura
  - SMTP port 25
  - POP3 port 110...

---

#### 3.2. FIREWALLS AMB CONTROL D’ESTAT (STATEFUL)

És la segona generació de tallafocs, que millora l’anterior.
- Tenen en compte, a més, **la col·locació de cada paquet individual dintre d’una sèrie de paquets**.
- Es coneix generalment com la “**inspecció d’estat de paquets**”, ja que manté registres de totes les connexions que passen pel tallafocs.

Pot determinar si un paquet concret indica:
- l’inici d’una nova connexió
- és part d’una connexió existent
- és un paquet erroni.

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-9f290d63.png" width="1000" />
</p>

<p align="center">
Font: https://infoproducts.alcatel-lucent.com/html/0_add-h-f/93-0267-HTML/7X50_Advanced_Configuration_Guide/AA-FW.html
</p>

---

Poden ajudar a **prevenir atacs contra connexions en curs o certs atacs de denegació de servei**.

**Símil:** A l'edifici d'una empresa arriba una pizza a la recepció. Si aquesta pizza havia estat demanada per algun empleat de l'edifici, el conserge la deixarà passar. En cas contrari, la caixa serà llençada per si de cas conté verí o una bomba.

---

#### 3.3. FIREWALLS DE NIVELL D’APLICACIO (APPLICATION LAYER)

Actuen sobre la **capa d’aplicació del model OSI**.
- Poden **entendre certes aplicacions i protocols** (ex: protocol de transferència de fitxers o FTP, DNS o navegació web)
- Permeten **detectar si un protocol no desitjat està funcionant per un port no estàndard o si s’abusa d’un protocol de forma perjudicial**.
- Molt **més segurs i fiables** comparat amb tallafocs de filtrat de paquets, ja que repercuteix sobre les 7 capes del model OSI.
- Similar als tallafocs de filtrat de paquets, però també **permet filtrar el contingut del paquet**.
  - Exemples: ISA (Internet Security and Acceleration) de Microsoft o Squid Proxy.
  - Exemple d’ús: Es pot blocar tota la informació relacionada amb una paraula clau, blocar per capçaleres HTTP, mida d’arxius, etc.
- Poden **filtrar protocols de capes superiors** com FTP, TELNET, DNS, DHCP, HTTP, TCP, UDP, TFTP, etc.

**Inconvenients**:
- Són més lents que els firewall d’estat i packet filter.
- Requereixen màquines més potents.

---

## LIMITACIONS DELS TALLAFOCS (DE QUE NO PODEN PROTEGIR)

En general, no protegeixen contra atacs que facin servir **tràfic que no passi per ell**.
- D’amenaces per **atacs interns** o d’usuaris negligents.
- Que espies corporatius **copiïn dades sensibles a mitjans físics d’emmagatzemament** i robar-les.
- Contra atacs d’**enginyeria social**.
- Contra atacs possibles a la xarxa interna per **malware arribat via fitxers** (tot i que pot filtrar si passen per ell).
- De **fallades de seguretat de serveis i protocols del que hem permès el tràfic**. Cal configurar correctament i cuidar la seguretat dels serveis que es publiquin a Internet.

---

Seguint amb el símil del guarda a la porta com a firewall, aquest no se n'adonarà si per exemple al lavabo de l'establiment hi ha problemes. Només veu les persones que entren i surten per la seva porta.

---

## CONFIGURACIO DE TALLAFOCS

Existeixen dos tipus de polítiques de configuració:
- **Permissiva**: Permetre tot el tràfic (ip’s, ports, protocols, etc) i blocar només el que no es vol.
- **Restrictiva**: No permetre cap tràfic de xarxa i aplicar excepcions al tràfic que compleix les condicions.

---

Seguint amb el símil del guarda a la porta, li diem:
- **Permissiva**: No deixis entrar els que portin sabatilles blanques. La resta, deixa'ls entrar. Se't pot colar tothom!!
- **Restrictiva**: Deixa entrar persones de cabell ros. La resta, no deixis entrar a ningú. Només passaran els explícitament permesos.

---

Sempre és **més segur el model Restrictiu**.

En general un tallafocs té **tres formes d'actuar** davant de cada paquet que vol passar per ell:
1. **Permetre (ACCEPT)**: El deixa passar en direcció al destí al que anava dirigit.
2. **Ignorar (DROP o DENY)**: Descarta el paquet, no el deixa passar.
3. **Rebutjar (REJECT)**: Descarta el paquet, no el deixa passar, i envia un missatge ICMP al host que el va enviar.

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-39f67883.png" width="500" />
</p>

<p align="center">
Possibles accions d'un firewall per a cada paquet
</p>

---

## ARQUITECTURES DE SEGURETAT AMB FIREWALLS

Tenim diferents arquitectures per configurar la seguretat perimètrica. Les més habituals:
1. Arquitectura 1 router
2. DMZ de host
3. Dual homed host (o gateway)
4. DMZ amb un sol firewall (3 legged model)
5. DMZ amb dos firewalls

---

### 1. ARQUITECTURA 1 ROUTER

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-46785a76.png" width="600" />
</p>

Xarxa local o intranet darrera d'un router: tot el tràfic de sortida està permès.
- Usat en **petites xarxes** com les domèstiques. La funció de firewall la fa el router ADSL o de fibra.
- Tenim **un sol router que permet normalment totes les peticions** que s’envien des de la xarxa interna.
- Funciona amb **redirecció de ports per a reenviar peticions externes cap a servidors de la xarxa interna**.
   - Per exemple, quan tenim una consola de jocs que volem que faci de servidor de partides, li hem de reenviar les peticions de connexió que arriben al router extern destinades al port del servidor de partides.

 Característiques:
 - Tenim una xarxa plana **sense segmentar**.
 - Es publiquen els serveis interns.
 - No existeixen elements de monitoratge.
 - Es tanca el tràfic originat des de l'exterior, però no el que es genera des de l'interior.
 - **No es verifica malware o spam** en el correu electrònic.
 - Els clients remots accedeixen directament als serveis de la xarxa local.

---

**NOTA**: La fletxa indica el sentit d'inici de la connexió (la punta de la fletxa indica quin és el host o xarxa destí). El color verd indica tràfic permès i el vermell, tràfic prohibit.

---

### SEGURETAT PERIMÈTRICA: ARQUITECTURES AMB DMZ

- **DMZ: Zona desmilitaritzada**. Xarxa perimètrica ubicada entre la xarxa interna i una xarxa externa, normalment Internet.

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-e3c14582.png" width="600" />
</p>

<p align="center">
Zona desmilitaritzada entre les dues Corees (del nord i del sud)
</p>

**Objectiu**: Les connexions iniciades des de la xarxa Externa cap a la DMZ estan permeses, però no les iniciades a l’externa cap a la interna o iniciades des de la DMZ cap a la interna.
- Els equips de la xarxa DMZ donen serveis a l’externa (són normalment **servidors públics**) i es protegeix la xarxa interna en cas que els servidors siguin compromesos.

---

Un **símil de DMZ** podria ser el rebedor de casa nostra. Quan arriba un paquet, no deixem entrar a casa el repartidor. El rebem al rebedor, tanquem la porta que dona accés a casa i així no entrarà ningú fins la cuina, ni veurà com és casa meva!

---

### 2. DMZ DE HOST (BASTIO)

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-3f476114.png" width="600" />
</p>

Aquest model tindrà una DMZ virtual:
- No hi ha separació o segmentació física entre les xarxes (només tenim una xarxa interior).

El **Router/Firewall reenvia les peticions externes cap a un sol host, anomenat Bastió**.
- Alguns routers domèstics ho permeten en la seva configuració i es tracta de reenviar tot el tràfic que s'origina a l'exterior cap a un sol host que contindrà els serveis que volem exposar (per exemple una consola de jocs).
- Es pot configurar el host bastió com a firewall proxy i obligar els hosts de la xarxa a passar per ell via configuració dels programes clients.
- El host bastió pot accedir a la xarxa interna i per tant no és tant segur com una DMZ amb 3 potes o amb 2 firewalls.
- Tot el tràfic entre xarxa interna i externa prohibit (vermell). Tot el tràfic haurà de passar pel bastió, que filtrarà el que no es vol.

---
**NOTA**: La fletxa indica el sentit d'inici de la connexió (la punta de la fletxa indica quin és el host o xarxa destí). El color verd indica tràfic permés i el vermell, tràfic prohibit.

---

#### BASTIÓ

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-2fb37403.png" width="400" />
</p>


Les accepcions del concepte Bastió són:
1. **Reducte fortificat** que es projecta cap a l'exterior del cos principal d'una fortalesa, situat generalment en els cantons dels murs (anomenats murs de cortina), com punt fort de la defensa contra l'assalt de tropes enemigues. És un terme militar.

2. **Aplicació que sol estar en un servidor** amb el fi d’oferir seguretat a la xarxa interna. És configurat especialment per la recepció d’atacs

---

### 3. DUAL HOMED HOST (O GATEWAY)

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-8b4dfac4.png" width="600" />
</p>


Es fa servir un host anomenat **Bastió**, que es troba **interceptant el tràfic entre el router i la xarxa interna**.
- Tot el tràfic entre la xarxa interna LAN i l'externa WAN està prohibit (vermell).
- Tot el tràfic haurà de passar per força pel host bastió, el qual filtrarà el tràfic que no es vol.
- No hi ha cap tipus de reenviament (en anglès, forwarding) de tràfic
- Només es permet connectar des de l'exterior al Bastió que fa de "proxy".
- No es permeten connexions directes entre xarxes LAN interna i l'externa.

D'aquesta manera s'amaga la xarxa interna dels possibles atacants.

---

**NOTA**: La fletxa indica el sentit d'inici de la connexió (la punta de la fletxa indica quin és el host o xarxa destí). El color verd indica tràfic permés i el vermell, tràfic prohibit.

---

### 4. DMZ AMB UN SOL FIREWALL (3 LEGGED MODEL)

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-22cf08ed.png" width="600" />
</p>

---

En aquest model, **tenim un sol firewall amb al menys 3 interfícies** de xarxa (xarxa externa, xarxa DMZ, xarxa interna).
- El firewall es converteix en un punt únic de fallada i ha de gestionar tot el tràfic per la DMZ i la xarxa interna.
- No es permet el tràfic directe entre xarxes externa i interna, tant l'originat a la xarxa externa com l'originat a la interna.
- El firewall es configura per a permetre el tràfic originat a la xarxa externa cap a la DMZ i l'originat a la DMZ cap a l'externa.

---
**NOTA**: La fletxa indica el sentit d'inici de la connexió (la punta de la fletxa indica quin és el host o xarxa destí). El color verd indica tràfic permés i el vermell, tràfic prohibit.

---

### 5. DMZ AMB DOS FIREWALLS

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-411298b0.png" width="600" />
</p>

En aquest model tenim 2 firewalls que separen les xarxes, deixant al mig la DMZ.
- Aquesta configuració **més segura** que l'anterior, ja que implica que un atac hauria de controlar els dos firewalls.
- Seria interessant que fossin de **diferents marques**, per evitar vulnerabilitats idèntiques als dos.
- El primer firewall (perimetral) es configura per a permetre el tràfic originat a la xarxa externa cap a la DMZ i l'originat a la DMZ cap a l'externa.
- El segon firewall (intern) es configura per a permetre el tràfic originat a la xarxa interna cap a la DMZ.

---

**NOTA**: La fletxa indica el sentit d'inici de la connexió (la punta de la fletxa indica quin és el host o xarxa destí). El color verd indica tràfic permés i el vermell, tràfic prohibit.

---

## ALTRES MESURES DE SEGURETAT PERIMETRAL: IDS I IPS

Per detectar tràfic maliciós a la xarxa local i al perímetre, podem fer servir **detectors d'intrusos a la xarxa o NIDS (Network Intrusion Detection System)**.

- Intrusion Detection Systems Explained: 12 Best IDS Software Tools Reviewed - https://www.comparitech.com/net-admin/network-intrusion-detection-tools/

N'hi ha de dos tipus:
- **IDS: Intrusion Detection System**. Dispositius que monitoritzen la xarxa i generen alarmes si es produeixen alertes de seguretat.
- **IPS: Intrusion Prevention System**. A més de l’anterior, bloquen l’atac evitant que tingui efecte.
- Informació sobre IDS i IPS:
	- https://www.rediris.es/cert/doc/unixsec/node26.html
	- https://www.incibe.es/empresas/blog/son-y-sirven-los-siem-ids-e-ips
---

### IDS i IPS

**Funcions**:

- **Identificació dels atacs**, quan treballa en mode passiu o IDS.
- **Registre d’events**: genera fitxers de log amb tot el que passi.
- **Bloqueig dels possibles atacs**, quan treballa en mode actiu o IPS.
- Informar els administradors de les anomalies (per correu-e, SMS, etc.)


#### TIPUS DE IDS

Segons on estigui el IDS tenim:

- **NIDS: Network IDS**: Equips que monitoren el tràfic de la xarxa.

- **HIDS: Host IDS**: S'instal·la a cada equip de la xarxa. Monitoritza canvis en el sistema operatiu i les aplicacions de l'equip local. Normalment envien les alertes i logs a un servidor central que els recopila i analitza.


<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-12941d64.png" width="600" />
</p>

<p align="center">
https://www.comparitech.com/net-admin/network-intrusion-detection-tools/
</p>

---

Existeixen peces de programari que milloren els HIDS. Són els **EDR (Endpoint Detection Response)**, que combinen l'antivirus tradicional juntament amb eines de monitoratge i intel·ligència artificial per a oferir una resposta ràpida i eficient davant els riscos i les amenaces més complexes. https://www.incibe.es/empresas/blog/sistemas-edr-son-y-ayudan-proteger-seguridad-tu-empresa

---

#### IDS VS FIREWALLS

Comparant els firewalls i els IDS:

**Els Firewalls**:
- examinen el tràfic extern per blocar tràfic atacant i han de situar-se en intercepció del tràfic, que ha de passar per ell.
- **No estan dissenyats per avisar**, tot i que guarden logs.
- Tampoc detecten atacs provinents de la xarxa interna (no el poden interceptar).

**Els IDS**:
- observen el tràfic de la xarxa i **detecten i avisen dels atacs**, inclús si aquests provenen de la xarxa interna, cosa que no pot fer el firewall.
- No cal que estiguin a una zona de intercepció del tràfic, poden ser un host més.
- A més els IPS poden tancar les connexions considerades atacs.
- Tenen un nivell d'intel·ligència més alt que els firewalls i miren patrons sofisticats de tràfic de xarxa.

---

### METODES DE DETECCIO D’INTRUSIONS: FIRMES I HEURISTICA

Com s'ho fan els IDS per a detectar possibles atacs de xarxa? Bàsicament hi ha dos mètodes per detectar els possibles atacs, de forma semblant al que fa l'antimalware:

#### FIRMES

S’analitzen els paquets de la xarxa i es comparen amb patrons d’atac coneguts i preconfigurats (firmes). Si es troba coincidència, s'envia una alarma.
- Cal preconfigurar les firmes i **només es detecten atacs coneguts, mai els nous**.
- Per exemple, miren seqüències de bytes determinades en el tràfic de xarxa, o una seqüència determinada d’instruccions usades per malware.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-a9984416.png" width="600" />
</p>

---

#### HEURISTICA

Es determina l’activitat normal de xarxa (com l’ample de banda usat, protocols, ports i dispositius que generalment s’interconnecten) i alerta a un administrador quan aquest varia del normal.
- Permet detectar atacs desconeguts, però pot generar falsos positius.
- **Exemple**: Emmagatzemar quins equips accedeixen als servidors de les BD de nòmines i alertar quan algú altre hi accedeix.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-baa0db49.png" width="600" />
</p>


---

### UBICACIO DELS IDS

La ubicació dels IDS és molt important:

- 1. **Darrera/davant el firewall**: Només detectaria atacs des de l’exterior.
- 2. Connectat a un **port d’inspecció del switch o mirall (mirror port)**:
  - **Monitorar tots els ports**: Veuria tot el tràfic, però cal anar en compte per que pot generar molt de tràfic a l’IDS. També es poden generar moltes alertes inexistents.
  - **Només monitorar ports de servidors**: Vigilem els atacs a servidors.
- 3. Usant un **TAP (test access point)**, que és un dispositiu físic que replica la comunicació que hi ha en un port, a mode de "lladre".

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-6ce0de40.png" width="400" />
</p>
<p align="center">
Network TAP (https://www.garlandtechnology.com/products/aggregator-tap-passive)
</p>


**Port mirror**: Mode de funcionament d'un port de switch de manera que les trames que arriben pels altres ports del switch són reenviades al port del monitor o IDS.  https://community.fs.com/es/article/port-mirroring-explained-basis-configuration-and-fa-qs.html

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-57f4e8b9.png" width="300" />
</p>

<p align="center">
Configuració per monitoritzar xarxa LAN
</p>

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-e7e411cc.png" width="600" />
</p>

<p align="center">
Possibles ubicacions dels IDS: (A) Tràfic extern, (B) tràfic de la DMZ, (C) i (D) Monitoritzar xarxes locals
</p>

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-bc74746a.png" width="600" />
</p>

<p align="center">
IDS vs IPS: El primer (esquerra) només envia alertes. El segon (dreta) és capaç de blocar tràfic (configurant el switch per treballar en equip amb el IPS)
</p>

---

### LIMITACIONS DELS IDS

Aquest aspectes limiten la seva utilització:
- **Soroll**: Els afecta molt el soroll que genrea falses alarmes (paquets generats per errors de programari, per exemple).
- **Falses Alarmes**: Hi ha moltes més falses alarmes que atacs reals, per tant poden fer que s’ignorin alarmes d’atacs reals.
- Les **firmes obsoletes** fan que no es detectin noves formes d’atacs. Cal actualitzar sovint.
- Hi ha un **retard entre el descobriment d’un tipus d’atac i la creació de la firma** corresponent.

A més:
- No poden evitar atacs fets contra autenticació dèbil.
- Molts no poden treballar amb paquets encriptats.
- Es refien de les adreces IP dels paquets, però molts atacs les falsifiquen (IP spoofing).
- Poden ser vulnerables als mateixos atacs orientats a protocols que els hosts de la xarxa i fer-los caure.

### SIEM

Un SIEM (Security Information and Event Management) és una solució que recopila, emmagatzema i analitza de manera centralitzada els registres dels diferents dispositius de maquinari i programari de la xarxa (des del perímetre a l'usuari final).
- Monitora les amenaces de seguretat en temps real
- Recull els registres d'activitat (logs) dels diferents sistemes.
- Relaciona els logs i detecta activitats sospitoses o inesperades que poden suposar l'inici d'un incident.
- Permet prendre ràpides mesures de detecció, contenció i resposta davant atacs
- Descarta els resultats anòmals (falsos positius) i genera respostes en base als informes i avaluacions que registra.
- Proporciona informes integrals de seguretat i administració del compliment.

Quan es produeix un atac en una xarxa equipada amb SIEM, el programari envia informació a tots els components de TI (portes d'enllaç, servidors, firewalls, etc.).

<p align="center">
<img src="imgs/firewalls-seguretat-perimetral-22d835c7.png" width="800" />
</p>

<p align="center">
Mapa lògic SIEM (https://www.researchgate.net/figure/SIEM-multivariado-distribuido_fig1_308970625)
</p>


---

### EXEMPLES DE NIDS (DE XARXA) OPEN SOURCE

https://protegermipc.net/2017/02/22/mejores-ids-opensource-deteccion-de-intrusiones/

#### SNORT

Projecte amb molts anys d'experiència, nascut a 1998. Té molt de suport de la comunitat. https://www.snort.org/

Característiques:
- La configuració és complexa (es fa des de consola)
- Existeixen algunes eines gràfiques com Snorby o Squil.
- Repositori de firmes bàsic gratuït

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-235e2625.png" width="200" />
</p>

---

#### SURICATA

NIDS molt semblant a Snort i compatible amb el seu sistema de firmes. https://suricata.io/

Avantatges:
- Execució multifil
- acceleració hardware amb GPU
- extracció de fitxers, per exemple de malware
- llenguatge d’scripts
-anàlisi de certificats TLS/SSL, etc.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-cecc4d24.png" width="200" />
</p>


---

#### BRO-IDS

Basat en firmes i anomalies, permet capturar tràfic per a crear el nivell base. Corva d’aprenentatge forta. Permet desconnectar atacants.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-8ff047f7.png" width="200" />
</p>



#### KISMET

IDS per a xarxes Wireless. Pot detectar AP’s falsos i evitar la suplantació d’AP’s avisant de la seva presència.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-fd52c30e.png" width="300" />
</p>

---

### EXEMPLES DE HIDS

#### OSSEC

Corre sobre qualsevol sistema operatiu i envia la informació a través de xarxa per evitar perdre-la en cas de rebre un atac.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-60ffb464.png" width="300" />
</p>


- Es pot instal·lar un servidor centralitzat per recollir les mostres de tots els agents instal·lats en els hosts.

#### Tripwire Opensource

Versió de la comunitat per a portar control d’intrusions i d’integritat dels arxius del sistema. Crea fitxers de hash i els guarda xifrats per anar comprovant si hi ha hagut modificacions no autoritzades.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-ecefafea.png" width="300" />
</p>

---

## Altres mesures de seguretat perimetral: Honeypots (Pots de mel)

Els Honeypots són sistemes dissenyats per a protegir la xarxa real d’un possible atac.
- Es fa servir d’esquer per a un atacant, de manera que es pot detectar l’atac abans que afecti sistemes crítics.
- Si es tracta d’una xarxa completa, es diuen Honey Net.

<p align="center">
<img src="imgs/seguretat-perimetral-firewalls-7620f901.png" width="500" />
</p>


**Objectius**:
- Alertar de l’existència d’un atac.
- Obtenir informació sense interferir en l’atac.
- Intentar alentir l’atac i protegir així la resta del sistema.

**Exemples d'ús**:
- Detectar malware
- Detectar spammers
- Enganyar i desgastar l’atacant
- Servidors de BD falsos per evitar atac al bo, etc.

### EXEMPLES DE HONEYPOTS

- **Deception toolkit**: http://www.all.net/dtk/ Es basa en implementar un equip que simula molts serveis amb vulnerabilitats conegudes. L’engany com a contramesura.

- **Kippo**: https://github.com/desaster/kippo Honeypot per a simular servidor SSH i detectar atacs de força bruta i la interacció SSH amb l’atacant.

- **Valhala**: https://sourceforge.net/projects/valhalahoneypot/ Honeypot Windows que simula http (web), ftp, tftp, finger, pop3, smtp, echo, daytime, telnet i port forwarding (alguns reals i altres simulats).

- **KFSensor**: http://www.keyfocus.net/kfsensor/ Comercial per a Windows. Preconfigurat per monitorar ports TCP i UDP, i ICMP. Configurat també per l'emulació de serveis comuns com ara http/https (web), ftp, pop3, smtp, xarxes Windows SMB, NetBIOS, CIFS, MySQL, telnet, terminal server, VNC... Envia alertes, té protecció contra DoS, etc.

---

# REFERENCIES I MES INFORMACIO A ESTUDIAR

- Seguretat perimetral: https://sergvergara.wordpress.com/2011/03/14/arquitectura-y-diseno-de-seguridad-de-red-perimetral/

- Tallafocs i DMZ: https://www.rediris.es/cert/doc/unixsec/node23.html

- Vídeo seguretat perimetral: http://www.criptored.upm.es/intypedia/video.php?id=introduccion-seguridad-perimetral&lang=es

- Informació sobre IDS i IPS:
https://www.rediris.es/cert/doc/unixsec/node26.html
http://slideplayer.es/slide/10920509/
http://slideplayer.es/slide/3588428/

- Honeypots i honeynets - https://curlie.org/Computers/Security/Honeypots_and_Honeynets/
