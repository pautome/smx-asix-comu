# Seguretat passiva (equips)

**INS Carles Vallbona**

**Pau Tomé** (Act oct-2023)

<p align="center">
<img src="imgs/2.seguretat-fisica-c8ae9cd5.png" width="500" />
</p>

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Seguretat passiva (equips)](#seguretat-passiva-equips)
	- [EMPLAÇAMENT DE LES INSTAL·LACIONS](#emplaament-de-les-installacions)
	- [AILLAMENT](#aillament)
	- [CONDICIONS AMBIENTALS](#condicions-ambientals)
	- [SUBMINISTRAMENT ELECTRIC I COMUNICACIONS](#subministrament-electric-i-comunicacions)
	- [CPD REDUNDANT (CENTRE “RESPALDO” O CR)](#cpd-redundant-centre-respaldo-o-cr)
	- [MESURES DE PREVENCIO INCENDIS](#mesures-de-prevencio-incendis)
	- [MESURES DE SEGURETAT FISICA](#mesures-de-seguretat-fisica)
		- [CLASSIFICACIO DE MESURES DE SEGURETAT FISICA](#classificacio-de-mesures-de-seguretat-fisica)
		- [1. DISSUASIVES](#1-dissuasives)
		- [2. DIFICULTAR L'ACCES A PERSONAL NO AUTORITZAT](#2-dificultar-lacces-a-personal-no-autoritzat)
		- [3. DETECCIO D'INTRUSOS](#3-deteccio-dintrusos)
- [ESTÀNDARD PER A CONSTRUIR CENTRES DE DADES](#estndard-per-a-construir-centres-de-dades)
- [REFERENCIES](#referencies)
	- [CASOS D'EXIT DE CPD](#casos-dexit-de-cpd)
	- [L'ESTANDAR TIA 942 I ELS TIER](#lestandar-tia-942-i-els-tier)

<!-- /TOC -->
---

- Vídeo "inside a Google Datacenter" - https://www.youtube.com/watch?v=XZmGGAbHqa0
- Vídeo Google Datacenter security 6 layers - https://www.youtube.com/watch?v=kd33UVZhnAA
- Vídeo Google Datacenter 360 tour - https://www.youtube.com/playlist?list=PLIivdWyY5sqJ9ufirPxjjc6uHpgJY_w6i

## EMPLAÇAMENT DE LES INSTAL·LACIONS

**CPD:** Centre de processament de dades, DataCenter, sala freda, “peixera”. És la sala on es centralitzen els servidors. S'ha de procurar ubicar en llocs que compleixin:

- **Baixa visibilitat:** Llocs de difícil accés (en cas d'haver de mantenir alt secret), evitar posar cartells i/o logos que indiquin que és un CPD i on està el CPD.

- **Ubicació i accés:** Primera planta edifici (evita inundació si està al soterrani, un atac si està a la planta baixa i incendi que puja si està a plantes superiors). Accés molt controlat. Passadís ample per poder entrar ordinadors grans.

- **Factors externs:** Proximitat de Policia, bombers, serveis sanitaris d'urgència. Baixa taxa de crim a la zona.  Empreses dels voltants sense activitat perillosa.

- **Sala:** Fals terra i fals sostre per distribuir cable i ventilació respectivament. Altura de la sala elevada, per poder apilar màquines en rack.

<p align="center">
<img src="imgs/2.seguretat-fisica-9d931f69.png" width="500" />
</p>
<p align="center">
<img src="imgs/2.seguretat-fisica-47287afa.png" width="500" />
</p>
<p align="center">
<img src="imgs/2.seguretat-fisica-90e7634a.png" width="500" />
</p>

- **Desastres naturals:** Evitar zones amb perill d'inundació, tornados, etc. També evitar terrenys amb perill de despreniments de roques o allaus.  

---

## AILLAMENT
Cal protegir els equips i persones de:  

- **Temperatura:** Els equips dissipen molta calor. Necessiten ventilació i una temperatura controlada.

- **Humitat:** Aigua i humitat ambiental alta poden provocar corrosió, i baixa electricitat estàtica. Potser necessari deshumidificadors.

- **Interferències electromagnètiques:** CPD lluny de màquines que poden generar-les (generadors elèctrics, maquinària industrial, fluorescents, etc).

- **Soroll:** Els ventiladors dels equips són molt sorollosos i cal aïllament acústic pels treballadors propers al CPD.

---

## CONDICIONS AMBIENTALS

<p align="center">
<img src="imgs/2.seguretat-fisica-e8fdfb90.png" width="400" align="center" />
</p>

<p align="center">
En aquest CPD es refigeren les cerveses del personal :)
</p>

---

- Els CPD **no solen tenir finestres** per evitar intrusos o pluja.

- **Equips de climatització**: Si pot ser, per duplicat. Mantenen la temperatura sobre els 22 graus i 50% d’humitat. Filtren l'aire en circuit tancat per protegir també contra la pols de l'aire.

http://www.cliatec.com/blog/condiciones-ambientales-para-cpd

- **Passadissos calents-freds:** Els equips es col·loquen en blocs formant passadissos. Tots els ventiladors orientats al mateix passadís. Allà es posen extractors de calor.

<p align="center">
<img src="imgs/2.seguretat-fisica-4bf063c6.png" width="400" />
</p>

<p align="center">
<img src="imgs/2.seguretat-fisica-a0bc0767.png" width="400" align="center" />

<p align="center">
Circulació de l'aire en passadissos del CPD
</p>

---

- **Altres sistemes**: Permeten refrigerar els racks directament amb unitats de climatització dintre del rack. Altres sistemes submergeixen l'electrònica dels servidors en líquid ignífug refrigerant (olis especials). També es pot refrigerar directament la CPU amb líquid.

https://www.rtcautomatismos.com/refrigeracion-de-equipos-por-inmersion-cpd/

---

## SUBMINISTRAMENT ELECTRIC I COMUNICACIONS

Cal assegurar l'electricitat pels equips i la connexió a xarxes externes:  

- **Redundància**: Tindrem dues empreses subministradores: Tant per el corrent elèctric com per a les telecomunicacions (en aquest cas, tecnologies diferents. Ex: fibra i ADSL).

- **Electricitat**: Circuit per alimentar els ordinadors separat de la resta de l'empresa.

- **SAI's (sistemes d'alimentació ininterrumpuda)**: Els SAI's són dispositius amb bateries que pemeten superar els possibles problemes elèctrics en cas de tall o de sobretensió o baixada de tensió.

<p align="center">
<img src="imgs/2.seguretat-fisica-f58b8b9a.png" width="400" align="center" />
</p>

<p align="center">
SAI d'un CPD
</p>


- **Generadors elèctrics (grups electrògens)**: Per sistemes crítics que no es poden aturar mai, per ex. en hospitals.

---

## CPD REDUNDANT (CENTRE “RESPALDO” O CR)

Per a grans empreses que no poden dependre de la fallada del seu CPD, creen un altre CPD anomenat CR amb una rèplica del primer:  

- **Físicament allunyat:** Per evitar que li afecti la mateixa contingència.

- **Stand-by:** El CR no sol donar servei fins que s'atura el CPD principal i proper.

- **Línies de comunicació de banda ampla:** Per poder estar sincronitzats.

- **Commutació:** Molt ben documentada per fer el canvi molt ràpid.

---

## MESURES DE PREVENCIO INCENDIS

Cal protegir les instal·lacions d'un possible incendi:

**Sistemes d'extinció:**
- Manuals: Extintors, mànegues d'aigua.
- Automàtics: Dispersors d'aigua o de gasos que o bé treuen l'oxigen de l'aire (el CO2) o bé interfereixen reaccions químiques de combustió (haló).

**Sistemes detectors:**
- Sensors per fum (sistemes òptics que detecten el fum per variacions de llum)
- Sensors d'augments desmesurats de temperatura.

**Sistemes manuals:**
- Alarmes d'incendi activades a mà.

- Dispersors de gasos.
<p align="center">
<img src="imgs/20180912-115822.png" width="500" align="center" />
</p>

- Aigua
<p align="center">
<img src="imgs/20180912-115846.png" width="500" align="center" />
</p>

- Detectors de fum
<p align="center">
<img src="imgs/20180912-115910.png" width="300" align="center" />
</p>

---

## MESURES DE SEGURETAT FISICA

La protecció física del CPD utilitza diferents mitjans.

- S'ha de **valorar amenaces i riscos** per determinar quines són les mesures de seguretat a instal·lar. Un alt nivell de seguretat pot suposar molt cost i impactar negativament en la productivitat.
- Les mesures de seguretat es posaran formant diferents capes de defensa.

Ex. Tanca perimetral + murs a les instal·lacions + accés mitjançant targeta i biometria + vigilància de guardies de seguretat + sensors de presència + càmeres de vídeo al CPD

---

### CLASSIFICACIO DE MESURES DE SEGURETAT FISICA

- Mesures de seguretat:
  1. 	Dissuasives
  2. 	Dificultar l'accés a personal no autoritzat
  3. 	Detecció d'intrusos

### 1. DISSUASIVES

Elements de seguretat visibles que fan que els atacants pensin que no és tan fàcil l'atac:  
- Sistemes d'alarma i cartell anunci: Avisar que hi ha una alarma.
- Guardes de seguretat.
- Gossos vigilants.
- Tanques al voltant de l'edifici.
- Il·luminació nocturna.

### 2. DIFICULTAR L'ACCES A PERSONAL NO AUTORITZAT

Els mètodes són bàsicament:

- Cadenes, cadenats, panys.
- Controls d'accés:
	- **Biomètrics**: Per a autenticar qui accedeix.
	- **Targeta intel·ligent**: Amb banda magnètica o altres.
	- **Teclat numèric**: per introduir un codi d'identificació.
	- **Seguretat perimetral**: com ara tanques per evitar l'entrada de persones.
	- **Mantraps**: Habitació amb dues portes. Es pot entrar lliurement per la primera. Un cop dins l'habitació, per passar la segona ha de passar una autenticació robusta. Si no, la persona queda tancada.

	<p align="center">
	<img src="imgs/20180912-120442.png" width="200" align="center" />
	</p>

---

### 3. DETECCIO D'INTRUSOS

Els mètodes són bàsicament:
- **Sensors de detecció interns**: Detecten canvis en l'ambient, que poden ser lumínics, sonors, de moviment...
- **Sensors de detecció externs**: sensors perimetrals.

**Cal que tinguin sistemes d'alimentació elèctrica independents de la principal i amb bateries, per que no deixin de funcionar si hi ha una apagada.**

---

# ESTÀNDARD PER A CONSTRUIR CENTRES DE DADES

A la Norma TIA 942 (datacenter) s'especifiquen els requisits estàndard per a CPD's. Podeu consultar en què consisteix a https://www.c3comunicaciones.es/data-center-el-estandar-tia-942/
- Es categoritzen els CPD pels nivells TIER, que indiquen les capacitats i instal·lacions dels CPD.
- Els nivells TIER van de 1 a 4, sent el 4 el millor. Actualment s'ha afegit el nivell 5.     

---

# REFERENCIES

## CASOS D'EXIT DE CPD

- ABAST Nuevo CPD de la CNMC: https://www.youtube.com/watch?v=bZi2GOrdmMg
- Data Center de Kamax - Caso de éxito de ABAST: https://www.youtube.com/watch?v=vXkIwuB-nMs
- Data center de DKV Seguros - caso de éxito de ABAST: https://www.youtube.com/watch?v=fyU8ihDru3k
- Nuevo Data Center de Agrupació - caso de éxito de Abast Systems: https://www.youtube.com/watch?v=8oekT7bJWHI
- Data Center MMT seguros - caso de éxito de Abast Grup: https://www.youtube.com/watch?v=donkgJfl5Y8
- Data Center Hermanos Fernández López - caso de éxito de Abast Grup: https://www.youtube.com/watch?v=HvgdTuFeSjc


## L'ESTANDAR TIA 942 I ELS TIER

- Norma TIA 942 datacenter: https://www.c3comunicaciones.es/data-center-el-estandar-tia-942/
- Els nivells TIER - https://blogs.salleurl.edu/es/el-estandar-tia-942-y-los-tier
- ANSI TIA 942-B (actualització) - https://www.optical.pe/blog/que-es-el-estandar-ansi-tia-942-b-y-para-que-sirve-en-tu-empresa/

- Nivells TIER - https://hoffman-latam.com/blog/clasificacion-tier-en-el-centro-de-datos-el-estandar-ansi-tia-942/
