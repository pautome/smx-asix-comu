# SAI'S

**INS Carles Vallbona**

**Pau Tomé** (Act Oct 2023)

<img src="imgs/2.seguretat-fisica-f58b8b9a.png" width="500" align="center" />

SAI d'un CPD


---

---

Els SAI's són dispositius amb bateries que pemeten superar els possibles problemes elèctrics. Existeixen dos tipus genèrics:
- SAI online
- SAI offline o standby

---

## SAI'S ONLINE

**SAI online:** Treballen de manera continuada, alimenten **sempre des de la bateria**.
- No tenen «temps de transferència» (interval de temps en que hi ha un microtall per commutar de llum a bateria).
- Són més cars.

<img src="imgs/2.seguretat-fisica-52f6c6b0.png" width="500" align="center" />

Font: https://download.schneider-electric.com/files?p_Doc_Ref=SPD_SADE-5TNM3Y_ES

---

## SAI'S OFFLINE O STANDBY

**SAI offline o standby:** Treballen només quan detecten un tall de corrent, alimenten des de la línia elèctrica i només de bateria quan hi ha un tall.
- Més econòmics però introueixen un «temps de transferència», que produeix un microtall quan falla el corrent elèctric.
- Alguns dispositius més sensibles queden afectats pel microtall i poden per exemple reiniciar-se.

<img src="imgs/2.seguretat-fisica-e4507001.png" width="500" align="center" />

Font: https://download.schneider-electric.com/files?p_Doc_Ref=SPD_SADE-5TNM3Y_ES

Diferències SAI online/inline/offline - https://www.tecnozero.com/blog/el-sai-offline-interactivo-online/

---

### COMPONENTS DELS SAI'S

Els components dels SAI's són:
- **Rectificador:** Converteix CA en CC.

- **Inversor:** Converteix CC en CA.

- **Bateria:** Magatzem d'energia. A més mida, més capacitat.

- **Interruptor principal:** Per apagar el SAI.

- **Connectors de corrent de sortida:** Endolls per connectar els dispositius de càrrega. Vigilar que n'hi ha NO alimentats per bateria.

- **Programes de control i monitoratge:** Programari per instal·lar a l'ordinador. Es comunica amb el SAI via Sèrie o USB. En el cas de múltiples ordinadors alimentats pel mateix SAI, funcionen en mode client-servidor i informen via xarxa de l'estat del SAI als ordinadors.

- **Indicadors d'estat:** LED's d'estat i avisos sonors (per ex. pèrdua de corrent). Indiquen:
	- **Línia / Bateria:** Funciona amb corrent / o bateria.
	- **Sobrecàrrega (overload):** Més càrrega de la permesa.
	- **Error de bateria:** Cal substituir-la.

<img src="imgs/20180912-121722.png" width="400" align="center" />

---

### FACTORS A CONSIDERAR EN SAI'S

Cal tenir en compte els següents factors a l'hora d'adquirir un SAI:
- **Mida del SAI:** Dimensions físiques del SAI.

- **Tipus de SAI:** Online o StandBy.

- **Càrrega del SAI:** Conjunt d'equips que té connectats. NO és el corrent que poden emmagatzemar a les bateries.

- **Autonomia (de les bateries):** Quantitat de temps que podrà subministrar energia, amb una càrrega determinada. A més càrrega, menys autonomia. Aquest temps ha de permetre fer un tancament ordenat de l'equip si no torna el corrent en temps curt.

- **Capacitat:** Potència màxima que pot subministrar a la càrrega. Mai no superar amb la càrrega la capacitat del SAI.

- **Potència aparent:** En VA (Voltamperes).

- **Potència real:** En W (watts).

---

**Exemple**: Tenim una càrrega de 400 W. Amb un SAI de capacitat 300 W / 500 VA no en tindrem prou (la potència real és de 300 W encara que l'aparent sigui 500 VA). Vigilar les unitats.

---

**NOTA:** Mai no connectar una impressora làser a un SAI. Fan servir resistències d'alt consum.

---

### Manteniment del SAI

Cal monitoritzar el SAI per assegurar que les bateries funcionen bé (es degraden amb el temps).
- El programari del SAI permet monitoritzar les bateries i aplicar operacions de descàrrega controlada.
