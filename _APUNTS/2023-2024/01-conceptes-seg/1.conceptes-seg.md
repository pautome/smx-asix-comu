# Conceptes sobre Seguretat informàtica

**INS Carles Vallbona**
**Pau Tomé**

![](imgs/1.conceptes-seg-81eedcd9.png)

(Act oct-2023)

---

- Vídeos Ciberseguretat en empreses (INCIBE) - https://www.youtube.com/watch?v=EHjmxujXIaQ&list=PLr5GsywSn9d_hd7MTimziiM8yZH1d-Igz&index=32
- Series ciberseguridad ataques y contramedidas Universidad Rey Juan Carlos - https://tv.urjc.es/series/580f3b4ad68b14293b8b456d?page=1

---

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Conceptes sobre Seguretat informàtica](#conceptes-sobre-seguretat-informtica)
	- [SEGURETAT INFORMATICA](#seguretat-informatica)
	- [PRINCIPALS OBJECTIUS DE LA SEGURETAT](#principals-objectius-de-la-seguretat)
	- [PER QUE VOLEM PROTEGIR?](#per-que-volem-protegir)
	- [QUE PROTEGIR? ELS ACTIUS](#que-protegir-els-actius)
	- [COM PROTEGIR ELS ACTIUS: LES MESURES DE SEGURETAT.](#com-protegir-els-actius-les-mesures-de-seguretat)
		- [CLASSIFICACIO DE LES MESURES DE SEGURETAT](#classificacio-de-les-mesures-de-seguretat)
		- [EXEMPLES AMENACES A LA SEGURETAT FISICA](#exemples-amenaces-a-la-seguretat-fisica)
		- [EXEMPLES AMENACES SEGURETAT LOGICA](#exemples-amenaces-seguretat-logica)
	- [OBJECTIUS DE LA SEGURETAT](#objectius-de-la-seguretat)
		- [CONFIDENCIALITAT](#confidencialitat)
			- [AUTENTICACIO](#autenticacio)
		- [INTEGRITAT](#integritat)
		- [DISPONIBILITAT](#disponibilitat)
		- [NO REBUIG (NO REPUDIO)](#no-rebuig-no-repudio)
	- [ORGANITZACIO	DE LA SEGURETAT EN CAPES](#organitzacio-de-la-seguretat-en-capes)
	- [ANALISI DE RISCOS](#analisi-de-riscos)
		- [EXEMPLE](#exemple)
		- [ESTRATEGIES PER ENFRONTAR RISCOS](#estrategies-per-enfrontar-riscos)
		- [CALCUL BASIC DE RISCOS](#calcul-basic-de-riscos)
	- [ATACS](#atacs)
		- [EXEMPLES DE TECNIQUES D'ATACS](#exemples-de-tecniques-datacs)
		- [TIPUS D'ATACANTS](#tipus-datacants)
	- [MECANISMES POPULARS DE SEGURETAT](#mecanismes-populars-de-seguretat)
		- [ALGUNES BONES PRACTIQUES DE SEGURETAT](#algunes-bones-practiques-de-seguretat)
	- [LEGISLACIO APLICABLE](#legislacio-aplicable)
	- [REFERENCIES](#referencies)

<!-- /TOC -->
---

## SEGURETAT INFORMATICA

La   seguretat   informàtica   consisteix   en:

 **Assegurar  que  els  recursos  del  sistema d'informació    d'una    organització    siguin utilitzats  de  la  manera  que  s'ha  decidit**  i que l'accés a la informació allà continguda, així   com   la   modificació,   **només   sigui possible   a   les   persones   que   es   trobin acreditades**  i  dintre  dels  límits  de  la  seva autorització.

~~~
“L ́únic  sistema  que  és  totalment  segur es    aquell    que    es    troba    apagat    
i desconnectat,   guardat   en   una   caixa forta  de  titani  que  està  soterrada  en ciment, envoltada  de  gas  nerviós
i  d'un grup de guàrdies fortament armats.

Tot i així, no les tindria totes.”

Eugene H. Spafford
[Expert en seguretat informàtica]
~~~

## PRINCIPALS OBJECTIUS DE LA SEGURETAT

La   seguretat   és   un   **problema   integral**, afecta a tot el sistema. Podríem  dir  que  els  problemes  no  poden ser  tractats  aïlladament,  degut  a  que  el  sistema es tant segur com el seu punt més feble.

- **Detectar els possibles problemes i amenaces** a la seguretat, minimitzant i gestionant els riscos.

- Garantir  l'**adequada  utilització**  dels  recursos  i de les aplicacions dels sistemes.

- **Limitar  les  pèrdues**  i  aconseguir  l'adequada **recuperació del sistema** en cas d'un incident de seguretat.

- Complir  amb  el  **marc  legal**  i  amb  els  requisits imposats a nivell organitzatiu.

---

## PER QUE VOLEM PROTEGIR?

No protegir la nostra informació ens pot portar a les següents pèrdues, entre d'altres:
- temps ja que caldrà restaurar la informació i equips perduts.
- prestigi ja que ningú es refiarà de nosaltres.
- dades ja que es poden vendre o usar per a fer xantatge.
- diners.
- drets i llibertats individuals degut a la pèrdua de les dades personals.

---

## QUE PROTEGIR? ELS ACTIUS

**Actius**: Són els recursos de l'empresa necessaris per desenvolupar les activitats diàries. Si no estan disponibles o es deterioren suposen un cost econòmic. Oviament cal protegir-los.

Disposen de diferents tipus d'actius i per a cada tipus haurem de prendre certes mesures. Com a exemples tenim:

- **Equips informàtics**: Ordinadors, dispositius de xarxa, mòbils, etc.
  - Cal evitar el seu robatori.
  - Cal fer manteniment preventiu, netejant periòdicament els equips.
  - Cal xifrar els discos que contenen.

- **Aplicacions**: Programes informàtics instal·lats als equips.
  - Cal Tenir les mínimes aplicacions possibles per evitar vulnerabilitats.
  - Cal clonar el programari dels pc's per facilitar-ne la gestió.
  - Cal evitar que els usuaris instal·lin res.
  - Cal assegurar la procedència de fonts legítimes de les aplicacions. No instal·lar res "pirata".

- **Dades**: Informació emmagatzemada als equips.
  - Cal evitar pèrdua i/o robatori, per exemple provocada per l'espionatge industrial.
  - Cal protegir contra el malware, fer backups i xifrar-les.

- **Comunicacions**: Xarxes i altres mitjans de transmissió.
  - Cal tenir canals xifrats per enviar dades.
  - Controlar l'accés remot als equips.
  - Evitar l'spam i la publicitat.
  - És molt important quan es treballa al núvol.

- **Usuaris**: Les persones que treballen a l'empresa.
  - Cal protegir les seves dades personals i sensibles.
  - Cal gestionar els seus privilegis sobre el sistema.
  - Cal controlar el seu accés físic al sistema.

---

## COM PROTEGIR ELS ACTIUS: LES MESURES DE SEGURETAT.

### CLASSIFICACIO DE LES MESURES DE SEGURETAT

1. Segons els recursos a protegir tenim:
  - **Seguretat física**: Són barreres i mecanismes de control que no permeten l'accés per contacte. Es sol identificar amb la seg.passiva.
  - **Seguretat lògica**: Mesures que asseguren programes i dades i l'accés d'usuaris al sistema. Sol ser seg. Activa

2. Segons quan actuen:
  - **Seguretat passiva**: Mesures que una vegada produït l'atac, minimitzen els efectes i recuperen l'estat anterior. Relacionat amb la **resiliència** (capacitat del sistema de recuperar-se després d'un desastre).
  - **Seguretat activa**: Mesures per detectar amenaces i evitar el problema, abans que es produeixi.

	Exemples: Si detecto que algú em vol pegar, una mesura de seguretat activa seria apartar-me per no rebre el cop o fins i tot contraatacar. Si resulta que m'han colpejat, una mesura de seguretat passiva és curar-me la ferida i posarme una bena.

---

### EXEMPLES AMENACES A LA SEGURETAT FISICA

- Desastres naturals
- Incendis i inundacions
- Robatoris
- Fallades de subministrament (apagades i sobretensions)
- Fallades de maquinari

### EXEMPLES AMENACES SEGURETAT LOGICA

- Virus, Troians i malware en general.
- Pèrdua de dades davant mal funcionament de programari.
- Atacs humans a aplicacions dels servidors: locals o remots aprofitant vulnerabilitats.
- Errades humanes dels usuaris del sistema.

---

## OBJECTIUS DE LA SEGURETAT

~~~
(ISO 27002: codi internacional de bones pràctiques de seguretat de la informació)
~~~

https://www.redseguridad.com/especialidades-tic/normativa-y-certificacion/sabes-diferenciar-la-iso-27001-y-la-iso-27002_20120220.html

Hi ha 4 conceptes que hem de tenir molt clars quan parlem de seguretat informàtica. Aquests seran els objectius primaris a assegurar:

  - **Confidencialitat**: Només poden accedir a la informació les persones autoritzades. També és coneix com a **secret**.
  - **Integritat**: La informació es manté completa i sense errors, i no pot ser alterada sense consentiment o autorització.
  - **Disponibilitat**: La informació ha de ser accessible pels usuaris autoritzats en un horari autoritzat.
  - **No rebuig**: Quan es produeix un intercanvi d'informació, no es pot negar haver participat en ella.

---

### CONFIDENCIALITAT

Per assegurar la confidencialitat, tabé conegut com a secret, es requereix:

- **Autenticació**: Confirmar que la persona és qui diu que és. Ex: targeta + PIN, usuari + contrasenya, etc.
- **Autorització**: Un cop autenticat, els diferents usuaris tindran diferents privilegis. Ex: entrada port Aventura (autenticació, per entrar al parc) + Fast Pass Port Aventura (autorització per passar davant a la cua).
- **Xifrat**: La informació està ofuscada per si un usuari no autenticat hi accedeix.

#### AUTENTICACIO

Per assegurar l'autenticació cal **demostrar que ets qui dius ser**. Es fan servir tres categories de mètodes (Saps-tens-ets):

- Una **cosa que SAPS**: Es necessita conèixer alguna cosa per autenticar-se. Per exemple, es demostra que ets un determinat usuari si es coneix el password associat a el seu nom d'usuari,

- Una **cosa que TENS**: Cal tenir un objecte en particular per demostrar la identitat. Exemple: els empleats d'una empresa tenen una targeta identificativa.

- Una **cosa que ETS**: Es reconeix alguna característica física del subjecte per autenticar-lo. Exemple: hi ha característiques fisiològiques com l'empremta dactilar o la retina que són úniques en cada persona. També es por utilitzar comportaments del subjecte com ara fer un reconeixement de veu o de la forma de caminar per autenticar.

---

### INTEGRITAT

Per assegurar la integritat, es requereix que **els canvis no autoritzats que es puguin produir en les dades durant el seu trànsit o emmagatzemament siguin detectables**.

- Això s’aconsegueix bàsicament mitjançant la **criptografia i el control d’accés**.
  - En el primer cas, es calcula un valor únic anomenat HASH per un fitxer que en el cas de que aquest fitxer sigui modificat encara que sigui un sol bit, canviarà també.
  - El control d'accés deixa accedir a la informació només als usuaris autoritzats.

---

### DISPONIBILITAT

Es requereix que el sistema sigui **tolerant a fallades**, o sigui, que tot i que alguna part del sistema falli, aquest sistema continuï donant servei.
- Això s’aconsegueix normalment mitjançant la **redundància** a diferents nivells: duplicant dades o discos o màquines o dispositius, etc.

---

### NO REBUIG (NO REPUDIO)

- Es requereix que es pugui autenticar sense dubte a l’emissor i/o el receptor (no rebuig en origen o destí).
- Això s’aconsegueix normalment mitjançant la **criptografia de clau pública i Autoritats de certificació**.

---

## ORGANITZACIO	DE LA SEGURETAT EN CAPES

Per a poder organitzar millor la defensa es va proposar un model que organitza les defenses en **capes, en nivells o barreres** que l’intrús es va trobant en el seu camí cap a l’obtenció de les dades.

Això aporta alguns aventatges:
- Augmenta les opcions de **detectar intrusos**.
- Permet **observar els atacants** per aprendre d'ells i defensar-se.
- Es descoratja i s'**alenteix el seu atac** i es guanya temps per a la defensa.
- Disminueix les possibilitats que els intrusos aconsegueixin el seu objectiu.
- És **més fàcil implementar el sistema de seguretat**, ja que permet dividir la  defensa en objectius més fàcilment implementables i controlables.

És important centrar-se en cada un d'aquests nivells per separat i aconseguir el nivell més alt possible de seguretat.

<img src="imgs/20180612-102244.png" width="600" align="center" />
<img src="imgs/20180910-095639.png" width="600" align="center" />

Les capes es poden exemplificar en una historieta:

1. Tenim un tresor dintre la torre central de la nostra fortalesa. Són les **dades** i les volem protegir dels atacs dels exèrcits enemics.

2. Aquestes dades són gestionades i es poden accedir-les mitjançant les **aplicacions**. Si volem que no es pugui accedir a les dades sense autorització, aquestes aplicacions han de ser segures i sense debilitats o vulnerabilitats que l'enemic pugui explotar.

3. Les aplicacions s'executen en un **Host o ordinador**. Aquest host sempre tindrà un sistema operatiu en concret (per exemple Windows o linux). Ens cal que aquest sistema operatiu no tingui debilitats que l'enemic pugui aprofitar.

4. Avui dia, els hosts o ordinadors no estan aïllats, s'han de comunicar amb d'altres ordinadors. Això es fa amb una **xarxa interna** que té un abast reduït, normalment una LAN. Si els enemics entren a aquesta xarxa, cal poder detectar-los i eliminar-los.

5. Però no s'acaba aquí la cosa: La nostra companyia s'ha de comunicar amb altres entitats o empreses. La nostra xarxa interna s'ha de connectar amb d'altres. Apareix el **perímetre** o frontera amb d'altres xarxes, com ara internet. Aquest punt d'unió podria ser aprofitat per l'enemic per infiltrar-se i accedir a la nostra xarxa local.

6. Per últim, el nivell de seguretat **físic** assegura l'accés a qualsevol element dels nivells anteriors de forma física. Els nostres actius poden estar en perill si l'enemic els pot agafar, robar-los o destruir-los.

7. Com que a la nostra organització hi haurà persones treballant-hi, cal que tots sàpiguin com comportar-se i siguin conscients que hi ha perills a fora. Tenim un nivell final de **directives, procediments i conscienciació** que no té aplicació tècnica, si no que implica un conjunt de documents normatius i actuacions de formació per evitar el desconeixement dels riscos.

---

Exemples de les mesures de seguretat que es solen aplicar a cada capa:

1. **Dades**: Protecció de les dades emmagatzemades. Normalment apliquem ACL’s i xifrat.

2. **Aplicacions**: Es tracta de reforçar el control de les aplicacions a l’equip local. Normalment, antivirus i antimalware.

3. **Equip local (host)**: Aquí es reforçarà el sistema operatiu. S’hi apliquen mesures com l’administració d’actualitzacions, l’autenticació i HIDS (Host Intrusion Detection System).

4. **Xarxa interna**: Per protegir-la, organitzem la xarxa interna fent segmentació de la xarxa, posant IDS per a detectar intrusions, i fem servir protocols de xarxa segurs com ara IPSec.

5. **Protecció perimetral**: Protegim l’accés des d'altres xarxes (per exemple Internet) a la nostra xarxa local. Apliquem Firewalls, DMZ (DeMilitarized Zone) i VPN (Virtual Private Network) entre d’altres.

6. **Protecció física**: Protegim l’accés físic als nostres actius. Per exemple amb guàrdies de seguretat, sistemes de bloqueig físic (portes, teclats amb codi, cadenats, etc.), dispositius de vigilància i seguiment, etc.

7. **Polítiques, procediments i conscienciació d’usuaris**: Intentem evitar atacs des de la pròpia organització ja sigui per desconeixement, error o de forma conscient i malintencionada. Dissenyem polítiques d’ús dels actius, procediments de sanció i programes de formació i aprenentatge per als usuaris.

---

## ANALISI DE RISCOS

Els actius que hem de protegir poden ser amenaçats per diferents factors. Podem distingir els següents conceptes bàsics:

- **Amenaces**: Factors (persones, màquines, successos) que atacaran el sistema si hi ha l'oportunitat. Ex: tallada elèctrica, cracker, fallada hardware. Poden ser:
	- **Intencionades:** persones que ens ataquen
	- **Fortuïtes:** succés natural o errada humana

- **Vulnerabilitat**: És un **defecte** d'una aplicació o d'un altre actiu (programari o maquinari). Ex: si no actualitzem el programari, és molt probable que s'aprofiti una fallada per a accedir sense autorització a un sistema.

- **Risc**: Probabilitat que una amenaça es materialitzi aprofitant una vulnerabilitat.
  - Una amenaça no suposa un risc si no hi ha vulnerabilitat.
  - Una vulnerabilitat no suposa un risc si no existeix l'amenaça.

- **Impacte**: Conseqüències que ens pot portar que una determinada amenaça es materialitzi realment, aprofitant una vulnerabilitat en un moment donat. L'impacte pot ser econòmic, de pèrdua de prestigi, de pèrdua de temps, etc.

### EXEMPLE

<img src="imgs/20180612-102956.png" width="200" align="center" />

- **Amenaça:** Atac d'un lleó.
- **Vulnerabilitat**: El nostre cos és dèbil, no tenim cuirasa.
- **Risc**: Si és a Granollers, molt baixa probabilitat de que es produeixi. Al Sub Sahara és molt més probable.
- **Impacte**: Ens pot ferir i fins i tot ens pot matar.

---

### ESTRATEGIES PER ENFRONTAR RISCOS

Davant un determinat risc podem optar per diferents estratègies:

- **Evitar-ho**: No fer servir actius que no siguin necessaris. Així minimitzem la possibilitat de rebre mal.
  - Per ex. no posar un servei web no essencial o desinstal·lar els  programes per defecte del sistema que no fem servir.

- **Assumir-ho**: Donem per fert que no podem o volem fer res. Això es fa si el cost d'aplicar mesures de seguretat és molt més gran que el mal que es pot rebre i/o també quan la probabilitat que es produeixi l'atac és molt baixa.
  - Per exemple, sortim al carrer i no ens protegim de la possible caiguda d'un piano a sobre nostre.

- **Aplicar mesures**: per reduir l'impacte o anul·lar-lo. Implica posar mesures de seguretat i coneixements tècnics. També un cost econòmic que a vegades l'organització no vol assumir o no val la pena assumir.
  - Per exemple, posar firewalls, antivirus, fer còpies de seguretat, posar càmeres de vigilància, etc.

- **Transferir el risc** a un altre, de manera que es cas d'atac, aquest altre es fa càrrec de la possible solució:
  - per ex. contractar una assegurança o fer un subcontracte amb una empresa de seguretat o contractar allotjament web a empreses especialitzades que et donen disponibilitat i seguretat per contracte.

---

### CALCUL BASIC DE RISCOS

La metodologia per a fer un anàlisi de riscos, a grosso modo, tindria els següents passos, que en realitat són un cicle:

1. Cal fer un **inventari de tots els actius** de l'empresa: Equips, Aplicacions, Dades, Comunicacions i Usuaris.

2. Cal fer un **llistat de totes les amenaces** que poden afectar els nostres actius.

3. **Per a cada amenaça, cal avaluar el risc**. Per exemple, podem posar valors de 0 a 3 (0=no probable, 1⁼poc probable, 2=mitjanament probable, 3=molt probable). També podriem posar valors de 0 a 5, etc.

4. **Per a cada amenaça que es pugui materialitzar, calcular l'impacte** (normalment en cost econòmic). Per exemple, valors de 0 a 3 (0=cost insignificant, 1⁼cost baix, 2=cost mitjà, 3=cost alt). Els valors econòmics depenen de l'empresa.

5. Calculem per a cada amenaça el **producte PxI** (exemple: Risc=2 i Impacte=2, PxI=4).

6. **Decidim a partir de quin valor de PxI posarem una mesura de seguretat** amb cost econòmic. Per exemple, a partir de PxI > 4 cal posar mesures, tenint en compte que els valors aniran de 0 a 9.

7. **Pensem les salvaguardes o mesures de seguretat** que posarem en el cas que es decideix posar una mesura. **ATENCIÓ: El cost de la mesura no hauria de ser superior al cost de l'impacte.**

8. Tornem al pas 1. per revisar tot el procès de nou. Ara cal incloure les salvaguardes o mesures de seguretat en el càlcul del risc.

Existeix programari per a fer aquestes tasques d'avaluació de riscos. Per exemple el programa "Pilar" de INCIBE https://www.incibe.es/protege-tu-empresa/catalogo-de-ciberseguridad/listado-soluciones/pilar

![](imgs/1.conceptes-seg-dbff7cea.png)

Pantalla de Pilar

---

Més informació detallada:
- Anàlisi de riscos - https://www.incibe.es/protege-tu-empresa/guias/gestion-riesgos-guia-empresario
- Vídeo Intypedia anàlisi de riscos - https://www.youtube.com/watch?v=EgiYIIJ8WnU

---

## ATACS

Un atac és la materialització del succés de l'amenaça contra un actiu. Es classifiquen en:

- **Atacs Actius**: Aquest tipus d'atacs modifiquen dades o bloquen i/o saturen canals de comunicació (Anomenats DoS = Denial of Service). Més fàcils de detectar per que es veuen els efectes.
  - Ex: Quan el professor explica i algú està parlant també, fa que la resta no escolti bé.

- **Atacs Passius**: En aquest tipus d'atac només s'accedeix a les dades, sense modificar-les. Són els més difícils de detectar.
	- Ex: Quan algú espia una conversa, tu no te n'adones.

---

Segons l'afectació que produeix l'atac, podem tenir:

- **Atac d'Interrupció**: Tall en la prestació d'un servei. (DoS)
<img src="imgs/20180612-103612.png" width="200" align="center" />

- **Atac d'Intercepció**: L'atacant pot copiar la informació que enviem (Man in the Middle en anglès).
<img src="imgs/20180612-103544.png" width="200" align="center" />

- **Atac de Modificació**: L'atacant a més d'interceptar, modifica les dades i provoca comportaments anormals.
<img src="imgs/20180612-103558.png" width="200" align="center" />

- **Atac de Fabricació**: L'atacant es fa passar per l'emissor de la transmissió (és una variació de l'atac de modificació).
<img src="imgs/20180612-103708.png" width="200" align="center" />

Classificació detallada http://recursostic.educacion.es/observatorio/web/fr/software/software-general/1040-introduccion-a-la-seguridad-informatica?start=5

---

### EXEMPLES DE TECNIQUES D'ATACS

- **Enginyeria social:** Mètode per aconseguir informació privilegiada sense usar cap eina tècnica, mitjançant deducció i manipulant els usuaris.
  - Ex: Demanar informació fent-se passar per algú amb influència a l'empresa.

- **Phishing:** S'envia un missatge amb un enllaç a una web falsa que es fa passar per una legítima, per robar credencials. Ex: missatge fals d'un suposat banc demanant canviar contrasenya i quan es posa, l'atacant la guarda. Després es reenvia a la pàgina original per que no es sospiti res.

![](imgs/1.conceptes-seg-8c3f514f.png)

---

- **Keyloggers:** Programa que permet capturar les tecles que es piquen al nostre equip i les envia a un atacant remot per analitzar-les i extreure'n contrasenyes, per exemple.

- **Atac de força bruta:** Consisteix en generar totes les possibles combinacions de contrasenyes i provar-les una per una.
  - Cal que el sistema permeti fer totes les proves **online**
  - També es pot obtenir prèviament el fitxer de passwords (hash) per poder provar tranquil·lament **offline** (fora de línia).

	Exemple: Un grup de lladres del Far West vol robar un banc. Tenen dues opcions:
	- intentar obrir la caixa dintre del banc durant el robatori (online)
	- emportar-se la caixa, anar a la muntanya i allà anar provant fins obrir-la o directament posar-hi dinamita.

- **Atac per diccionari:** Consisteix en generar combinacions de caràcters fent servir alguna informació que ens ajuda a reduir molt el nombre de proves.
  - Per exemple, es fa servir un llistat de paraules, com ara el fitxer "rockyou.txt", que conté contrasenyes fetes servir per altres usuaris i que es va filtrar per una errada de seguretat d'aquesta pàgina web.

- **Spoofing:** Consisteix en fer passar un equip per un altre, suplantar-ho. Això es fa, per exemple, usant la mateixa adreça MAC o IP de la víctima.

- **Sniffing:** Es Capturen els paquets que viatgen per la xarxa (cable o xarxa sense fils) i que en principi van destinats a algun altre equip.

- **DoS:** Denial of Service. Consisteix en saturar un servei des d'un sol equip per que funcioni lent o que deixi de funcionar totalment. Aquesta saturació es pot fer per exemple enviant moltes peticions molt ràpidament de manera que la víctima no podrà respondre.

- **DDoS:** Distributed DoS. Atac DoS coordinat des de diferents equips “Zombis” o controlats per un atacant. El grup d'equips zombi s'anomena "Botnet". Aquest atac és molt difícil de detenir.

---

### TIPUS D'ATACANTS

- **Hacker:** Persona amb molta curiositat i coneixements tècnics que ataca un sistema pel repte que li suposa. Si te èxit, avisaria el propietari per que ho solucioni, ja que és ètic.

- **Cracker:** Seria un hacker dolent, que ataca per fer mal: robar, sabotejar, riure-se'n d'algú, etc.

- **Script kiddie:** És un aprenent de Hacker i cracker que fa servir eines desenvolupades per altres sense saber que fan ni les conseqüències que te el seu ús.

- **Programadors de malware:** Experts programadors de sistemes operatius que desenvolupen malware. Actualment, guanyen molts diners.

- **Sniffers:** Experts en protocols de comunicació que poden obtenir informació analitzant els paquets de la xarxa.

- **Ciberterroristes:** Cracker amb interessos polítics i/o econòmics a gran escala. Solen atacar a nivell de païs.

---

## MECANISMES POPULARS DE SEGURETAT

Les mesures de seguretat més populars i que veurem són:
- **Redundància** (Backup i sistemes RAID)
- **Control d'accès** (alguna cosa que tinc, sé o soc)
- **Xifrat de dades, firma i certificats digitals** (criptografia)
- **Antivirus, antimalware i antispam**
- **Seguretat perimetral** (Tallafocs i elements similars).

---

### ALGUNES BONES PRACTIQUES DE SEGURETAT

- Determinar quins actius hem de protegir i definir la **política de backups**.

- Crear i revisar **plans de contingències** (actuacions davant catàstrofes)

- Instal·lar només els programes necessaris i **actualitzar-los regularment** i **automàticament**.

- **Revisar les llistes d'usuaris actius**, per si algun empleat ja no és a l'empresa o està de vacances.

- **Revisar llistes de control d'accés** d'usuaris, per no donar més privilegis dels necessaris.

- Donar **formació sobre seguretat** als usuaris.

- Apuntar-se a **llistes de correu, grups de telegram, discord, etc** sobre seguretat.

- **Revisar els logs del sistema**, fent servir eines d'ajuda.

- Fer **auditories externes**, per trobar errades que no veiem nosaltres mateixos.

- **Revisar la llista dels equips** connectats per si algun no és legítim.

- Activar **sistemes de monitorització i d'avís** de serveis.

---

## LEGISLACIO APLICABLE

- Existeixen una sèrie de lleis que regulen el tractament de dades i el comportament en sistemes digitals. Estableixen sancions en cas d'incompliment. Les més importants són:

	- **LOPD-GDD** (Llei Orgànica de Protecció de Dades Personals i de Garantia de Drets Digitals): Normativa espanyola derivada de l'europea RGPD que estableix com obtenir les dades (en qualsevol suport digital o no), com s'han de protegir i quins drets i obligacions té el ciutadà sobre les seves dades i davant tercers.

	- **LLSI-CE** (Llei de Serveis de la societat de la informació i el comerç electrònic): Estableix com funciona la prestació de serveis de comunicació electrònica (per ex. correu-e).

	- **LPI** (Llei de propietat Intel·lectual): Estableix els drets d'autor en entorns digitals i les sancions aplicables.

	- **Codi penal**: Bàsicament és un recull de delictes tipificats punibles amb multes i privació de llibertat. S'aplica a tots el ciutadans i conté referències a delictes informàtics.

---
## REFERENCIES

- Guia gestió de riscos - https://www.incibe.es/protege-tu-empresa/guias/gestion-riesgos-guia-empresario
