# Eines de test de xarxa v1.0

> NOTA: Els exercicis estan al final del document.

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Eines de test de xarxa v1.0](#eines-de-test-de-xarxa-v10)
	- [Referències](#referncies)
	- [1. Comprovar connectivitat](#1-comprovar-connectivitat)
		- [Ping](#ping)
		- [Ping6](#ping6)
		- [Dhcping](#dhcping)
		- [Arping](#arping)
	- [2. Comprovar ruta dels paquets](#2-comprovar-ruta-dels-paquets)
		- [Tracert (win), Traceroute (linux)](#tracert-win-traceroute-linux)
		- [Tracepath](#tracepath)
		- [Mtr](#mtr)
	- [3. Comprovar serveis (equip local)](#3-comprovar-serveis-equip-local)
		- [Netstat](#netstat)
		- [SS](#ss)
	- [4. Comprovar serveis (equip remot)](#4-comprovar-serveis-equip-remot)
		- [Telnet](#telnet)
		- [cURL](#curl)
	- [5. Comprovar DNS](#5-comprovar-dns)
		- [Nslookup](#nslookup)
		- [Dig](#dig)
		- [Dnstracer:](#dnstracer)
	- [6. Configurar i comprovar interfícies](#6-configurar-i-comprovar-interfcies)
		- [Ifconfig (linux), ipconfig (win)](#ifconfig-linux-ipconfig-win)
	- [iwconfig:](#iwconfig)
	- [7. Configurar i veure taula de rutes](#7-configurar-i-veure-taula-de-rutes)
		- [Route](#route)
	- [8. Arp](#8-arp)
	- [9. Paquet iproute2 i la comanda ip (linux)](#9-paquet-iproute2-i-la-comanda-ip-linux)
	- [10. Configuració manual i persistent de xarxa](#10-configuraci-manual-i-persistent-de-xarxa)
	- [11. Buscar a la xarxa](#11-buscar-a-la-xarxa)
		- [Nmap](#nmap)
	- [12. Altres configuracions de xarxa](#12-altres-configuracions-de-xarxa)
		- [Fitxer /etc/services i /etc/hosts](#fitxer-etcservices-i-etchosts)
	- [13. Subnetting](#13-subnetting)
		- [ipcalc](#ipcalc)
- [Exercicis](#exercicis)

<!-- /TOC -->

## Referències
http:// linux-ip.net/html/

- Tot seguit descriurem algunes eines per a fer tests a la xarxa.

---

## 1. Comprovar connectivitat

### Ping
- Comprovar la connectivitat de xarxa
- Mesurar la latència o temps que triguen en comunicar-se dos hosts remots.
- Conèixer l’adreça IP d’un nom de domini.

Exemples:

~~~
ping localhost
ping 127.0.0.1
ping www.google.es
~~~

https://norfipc.com/redes/usar-comando-ping.html

### Ping6

- Fa un ping en IPv6.

~~~
$ ping6 ::1
PING ::1(::1) 56 data bytes
64 bytes from ::1: icmp_seq=1 ttl=64 time=0.039 ms
64 bytes from ::1: icmp_seq=2 ttl=64 time=0.042 ms
~~~

### Dhcping

- Comprovar que el servidor DHCP funciona. Fa peticions DHCP al servidor

~~~
arping -I enp5s0 -c 2 192.168.0.1

- Detectar si una ip està en ús

$ arping -D -q -I eth0 -c 2 192.168.99.35
$ echo $?
1
$ arping -D -q -I eth0 -c 2 192.168.99.36
$ echo $?
0
~~~

http://linux-ip.net/html/tools-arping.html


### Arping
- Fa un “ping” però fent ser servir protocol ARP en lloc de ICMP. Només per a hosts a la mateixa xarxa local. Permet que un veí guardi entrades ARP (-U o -A).

~~~
$ arping -I enp3s0 172.16.9.3
ARPING 172.16.9.3 from 172.16.9.13 enp3s0
Unicast reply from 172.16.9.3 [08:60:6E:82:62:A8]  0.779ms
Unicast reply from 172.16.9.3 [08:60:6E:82:62:A8]  0.766ms
~~~

---

## 2. Comprovar ruta dels paquets

### Tracert (win), Traceroute (linux)

Visualitzar la ruta que segueixen els paquets per arribar al host destí. Es mostra una de les possibles rutes.
Hi ha salts que es fan per nodes diferents (es veuen 3 hosts en el salt 4, cadascun amb el seu temps de resposta, i 2 hosts en el salt 9)

~~~
$ traceroute www.google.es
traceroute to www.google.es (172.217.19.195), 30 hops max, 60 byte packets
 1  172.16.9.254 (172.16.9.254)  0.247 ms  36.110 ms  36.104 ms
 2  192.168.30.1 (192.168.30.1)  0.559 ms  0.652 ms  0.771 ms
 3  192.168.144.1 (192.168.144.1)  47.579 ms  47.705 ms  47.709 ms
 4  201.red-81-46-72.customer.static.ccgg.telefonica.net (81.46.72.201)  15.406 ms 193.red-81-46-72.customer.static.ccgg.telefonica.net (81.46.72.193)  14.343 ms 181.red-81-46-130.customer.static.ccgg.telefonica.net (81.46.130.181)  16.804 ms
 5  53.red-81-46-1.customer.static.ccgg.telefonica.net (81.46.1.53)  15.018 ms  15.009 ms  14.976 ms
 6  1.red-80-58-106.staticip.rima-tde.net (80.58.106.1)  13.845 ms  11.827 ms  15.139 ms
 7  * * *
 8  5.53.1.74 (5.53.1.74)  16.239 ms  16.210 ms  16.083 ms
 9  72.14.235.18 (72.14.235.18)  15.798 ms  15.654 ms 72.14.235.20 (72.14.235.20)  16.409 ms
10  216.239.41.237 (216.239.41.237)  32.585 ms  32.603 ms 216.239.41.19 (216.239.41.19)  30.813 ms
11  216.239.48.75 (216.239.48.75)  39.691 ms  39.744 ms  39.724 ms
12  209.85.142.74 (209.85.142.74)  39.156 ms 108.170.238.210 (108.170.238.210)  39.605 ms 209.85.143.210 (209.85.143.210)  38.669 ms
13  216.239.40.230 (216.239.40.230)  39.259 ms 209.85.255.215 (209.85.255.215)  39.123 ms 216.239.40.230 (216.239.40.230)  39.605 ms
14  * 108.170.241.193 (108.170.241.193)  38.175 ms  38.333 ms
15  72.14.238.245 (72.14.238.245)  36.427 ms  38.016 ms  37.855 ms
16  ams16s31-in-f3.1e100.net (172.217.19.195)  37.833 ms  36.874 ms  37.335 ms
~~~

### Tracepath

- Visualitzar la ruta que segueixen els paquets per arribar al host destí. Es mostra una de les possibles rutes.

  ~~~
  $ tracepath www.gogole.es
   1?: [LOCALHOST]                      pmtu 1500
   1:  mygpon                                               13.720ms
   1:  mygpon                                               13.814ms
   2:  mygpon                                               13.846ms pmtu 1492
   2:  no reply
   3:  ???                                                  17.238ms asymm  5
   4:  ???                                                 521.257ms
   5:  no reply
   6:  ae2-100-xcr1.bsb.cw.net                              17.107ms
   7:  ae20-xcr1.mar.cw.net                                 29.035ms
  ...
  ~~~

### Mtr
També mostra la ruta dels paquets, i s’actualitza en temps real.

~~~
mtr www.google.es
My traceroute  [v0.93]
pau-W65-W67RB (192.168.1.187)                          2021-03-22T20:24:25+0100
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
          Packets               Pings
Host                                Loss%   Snt   Last   Avg  Best  Wrst StDev
1. _gateway                          0.0%    46    0.6   0.5   0.4   0.7   0.1
2. mygpon                            0.0%    46    3.5   4.7   3.0  13.1   2.1
3. (waiting for reply)
4. 10.183.58.17                      0.0%    45    7.6   8.2   5.8  12.1   1.3
5. 172.29.88.113                     0.0%    45  642.6 570.4 535.0 645.0  27.6
6. 172.29.1.102                     90.9%    45    8.7   7.8   6.6   8.7   0.9
7. 172.29.1.101                     90.9%    45   16.9  17.1  16.6  17.8   0.5
8. 212.166.147.46                    0.0%    45   19.8  17.9  15.8  24.7   1.8
9. 172.253.50.53                     0.0%    45   17.5  17.2  15.5  24.7   1.5
10. 142.250.46.165                    0.0%    45   17.1  19.5  16.8  33.3   3.4
11. waw02s06-in-f3.1e100.net          0.0%    45   18.3  18.1  15.4  28.5   2.5

~~~
![](assets/test-xarxa-c539793b.png)

---

## 3. Comprovar serveis (equip local)

### Netstat

- Per mostrar les connexions que es fan des de i cap a l’equip local.
- Permet veure entre d’altres coses quin serveis estan escoltant en l’equip local.
- Té molts paràmetres per configurar la sortida.

- Mostra tots els ports de xarxa oberts amb el seu estat a l’equip local i quin procés el te obert.

- Els paràmetres més habituals seran "-atunp".

~~~
sudo netstat -atunp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name   

tcp        0      0 0.0.0.0:445             0.0.0.0:*               LISTEN      2042/smbd       
tcp        0      0 127.0.0.1:45635         0.0.0.0:*               LISTEN      2668/ica        
tcp        0      0 0.0.0.0:139             0.0.0.0:*               LISTEN      2042/smbd       
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1142/sshd       
tcp        0      0 172.16.9.13:139         172.16.9.3:60094        ESTABLISHED 3509/smbd       
tcp        0      0 172.16.9.13:139         172.16.9.6:59272        ESTABLISHED 3354/smbd       
tcp        0      0 172.16.9.13:139         172.16.9.5:33708        ESTABLISHED 3323/smbd       
tcp        0      0 172.16.9.13:139         172.16.9.21:34820       ESTABLISHED 2221/smbd       
tcp        0      0 172.16.9.13:139         172.16.9.23:54242       ESTABLISHED 3256/smbd       
~~~

### SS

- Eina alternativa a netstat, amb sintaxi similar.

~~~
ss | less // get all connections
ss -t // get tcp connections not in listen mode (server programs)
ss -u // get udp connections not in listen mode
ss -x // get unix socket pipe connections
ss -ta // get all tcp connections
ss -au // get all udp connections
ss -nt // all tcp without host name
ss -ltn // listening tcp without host resolution
ss -ltp // listening tcp with PID and name
ss -s // prints statstics
ss -tn -o tcp // connection with domain host and show keepalive timer
ss -tl4 // ip4 connections
~~~

---

## 4. Comprovar serveis (equip remot)

### Telnet

- Consola remota insegura si va al port 23.
- Comprovar si els serveis estan funcionant (posant el número de port del servei).
- Interacció bàsica amb serveis que funcionen amb missatges de text. Si es coneix el protocol, es poden escriure les comandes (exemple al servidor ftp).

- Accedir a servei Web

~~~
$ telnet www.google.es 80
Trying 172.217.19.195...
Connected to www.google.es.
Escape character is '^]'.

> Escrivim com a interacció
GET / (si no es una comanda vàlida del protocol donarà errors)

HTTP/1.0 400 Bad Request
Content-Type: text/html; charset=UTF-8
Referrer-Policy: no-referrer
Content-Length: 1555
Date: Wed, 18 Oct 2017 12:13:01 GMT

<!DOCTYPE html>
<html lang=en>
  <meta charset=utf-8>
  <meta name=viewport content="initial-scale=1, minimum-scale=1, width=device-width">
  <title>Error 400 (Bad Request)!!1</title>
  <style>
    *{margin:0;padding:0}html,code{font:15px/22px arial,sans-serif}html{background:#fff;color:#222;padding:15px}body{margin:7% auto 0;max-width:390px;min-height:180px;padding:30px 0 15px}* > body{background:url(//www.google.com/images/errors/robot.png) 100% 5px no-repeat;padding-right:205px}p{margin:11px 0 22px;overflow:hidden}ins{color:#777;text-decoration:none}a img{border:0}@media screen and (max-width:772px){body{background:none;margin-top:0;max-width:none;padding-right:0}}#logo{background:url(//www.google.com/images/branding/googlelogo/1x/googlelogo_color_150x54dp.png) no-repeat;margin-left:-5px}@media only screen and (min-resolution:192dpi){#logo{background:url(//www.google.com/images/branding/googlelogo/2x/googlelogo_color_150x54dp.png) no-repeat 0% 0%/100% 100%;-moz-border-image:url(//www.google.com/images/branding/googlelogo/2x/googlelogo_color_150x54dp.png) 0}}@media only screen and (-webkit-min-device-pixel-ratio:2){#logo{background:url(//www.google.com/images/branding/googlelogo/2x/googlelogo_color_150x54dp.png) no-repeat;-webkit-background-size:100% 100%}}#logo{display:inline-block;height:54px;width:150px}
  </style>
  <a href=//www.google.com/><span id=logo aria-label=Google></span></a>
  <p><b>400.</b> <ins>That’s an error.</ins>
  <p>Your client has issued a malformed or illegal request.  <ins>That’s all we know.</ins>
Connection closed by foreign host.

~~~

- Accedir al servei SSH (dona error)

~~~
$ telnet sdf.org 22
Trying 205.166.94.15...
Connected to sdf.org.
Escape character is '^]'.
SSH-2.0-OpenSSH_7.1
hi
Protocol mismatch.
Connection closed by foreign host.
~~~

~~~
$ telnet ftp.suse.com 21
Trying 195.135.221.132...
Connected to ftp.suse.com.
Escape character is '^]'.
220 "Welcome to ftp.suse.com"
> user anonymous
331 Please specify the password.
> pass 123
230 Login successful.
> quit
221 Goodbye.
Connection closed by foreign host.
~~~

- Aquí el servei no està disponible però el port no contesta i per tant queda esperant
~~~
$ telnet www.microsoft.com 21
Trying 2.19.168.244...
~~~

### cURL

- Com la navalla suïssa per a fer peticions a serveis. Permet comprovar el funcionament de molts serveis.


---

## 5. Comprovar DNS

### Nslookup

- Resoldre noms de domini i obtenir l’adreça IP (resolució directa).
- Resoldre l’adreça IP i obtenir nom de domini (resolució inversa).
- Obtenir registres i informació del servidor DNS.
- Té dos modes de funcionament: interactiu i no interactiu.

  - No interactiu

  ~~~
  $ nslookup www.google.es
  Server:        127.0.1.1
  Address:    127.0.1.1#53

  Non-authoritative answer:
  Name:    www.google.es
  Address: 172.217.19.195
  ~~~

- No interactiu indicant quin servidor DNS s’utilitza per resoldre (en aquest cas 8.8.8.8)

  ~~~
  $ nslookup www.iescarlesvallbona.cat 8.8.8.8
  Server:        8.8.8.8
  Address:    8.8.8.8#53

  Non-authoritative answer:
  Name:    www.iescarlesvallbona.cat
  Address: 149.202.75.48
  ~~~

- Resolució inversa

  ~~~
  $ nslookup 172.217.19.195
  Server:        127.0.1.1
  Address:    127.0.1.1#53

  Non-authoritative answer:
  195.19.217.172.in-addr.arpa    name = ams16s31-in-f3.1e100.net.
  195.19.217.172.in-addr.arpa    name = ams16s31-in-f3.1e100.net.

  Authoritative answers can be found from:
  ~~~

- Interactiu

  ~~~
  $ nslookup
  > www.google.es
  Server:        127.0.1.1
  Address:    127.0.1.1#53

  Non-authoritative answer:
  Name:    www.google.es
  Address: 216.58.212.131
  > www.iescarlesvallbona.cat
  Server:        127.0.1.1
  Address:    127.0.1.1#53

  Non-authoritative answer:
  Name:    www.iescarlesvallbona.cat
  Address: 149.202.75.48
  ~~~

### Dig

- Resoldre noms de domini.
- Obtenir registres i informació del servidor DNS

- El paràmetres són @servidor DNS, nom de host a resoldre i tipus de registre (aquí MX, ens mostrarà els servidors de Google).

  ~~~
  $ dig @8.8.8.8 google.es MX

  ; <<>> DiG 9.10.3-P4-Ubuntu <<>> @8.8.8.8 google.es MX
  ; (1 server found)
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20877
  ;; flags: qr rd ra; QUERY: 1, ANSWER: 5, AUTHORITY: 0, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 512
  ;; QUESTION SECTION:
  ;google.es.            IN    MX

  ;; ANSWER SECTION:
  google.es.        599    IN    MX    50 alt4.aspmx.l.google.com.
  google.es.        599    IN    MX    20 alt1.aspmx.l.google.com.
  google.es.        599    IN    MX    10 aspmx.l.google.com.
  google.es.        599    IN    MX    30 alt2.aspmx.l.google.com.
  google.es.        599    IN    MX    40 alt3.aspmx.l.google.com.

  ;; Query time: 36 msec
  ;; SERVER: 8.8.8.8#53(8.8.8.8)
  ;; WHEN: Wed Oct 18 14:09:56 CEST 2017
  ;; MSG SIZE  rcvd: 156
  Whois
  Permet obtenir informació del titular (propietari) d’una adreça IP.

  $ whois 81.46.72.197
  % This is the RIPE Database query service.
  % The objects are in RPSL format.
  %
  % The RIPE Database is subject to Terms and Conditions.
  % See http://www.ripe.net/db/support/db-terms-conditions.pdf

  % Note: this output has been filtered.
  %       To receive output for a database update, use the "-B" flag.

  % Information related to '81.46.0.0 - 81.46.127.255'

  % Abuse contact for '81.46.0.0 - 81.46.127.255' is 'nemesys@telefonica.es'

  inetnum:        81.46.0.0 - 81.46.127.255
  netname:        RIMA
  descr:          Telefonica de Espana SAU
  descr:          Red de servicios IP
  descr:          Spain
  country:        ES
  admin-c:        ATdE1-RIPE
  tech-c:         TTdE1-RIPE
  status:         ASSIGNED PA
  mnt-by:         MAINT-AS3352
  mnt-lower:      MAINT-AS3352
  mnt-routes:     MAINT-AS3352
  created:        2014-06-10T12:27:12Z
  last-modified:  2014-06-10T12:27:12Z
  source:         RIPE

  role:           Administradores Telefonica de Espana
  address:        Ronda de la Comunicacion s/n
  address:        Edificio Norte 1, planta 6
  address:        28050 Madrid
  address:        SPAIN
  org:            ORG-TDE1-RIPE
  admin-c:        KIX1-RIPE
  tech-c:         TTDE1-RIPE
  nic-hdl:        ATDE1-RIPE
  mnt-by:         MAINT-AS3352
  abuse-mailbox:  nemesys@telefonica.es
  created:        2006-01-18T12:24:41Z
  last-modified:  2014-04-23T17:23:39Z
  source:         RIPE - Filtered

  role:           Tecnicos Telefonica de Espana
  address:        Ronda de la Comunicacion S/N
  address:        28050-MADRID
  address:        SPAIN
  org:            ORG-TDE1-RIPE
  admin-c:        TTE2-RIPE
  tech-c:         TTE2-RIPE
  nic-hdl:        TTdE1-RIPE
  mnt-by:         MAINT-AS3352
  abuse-mailbox:  nemesys@telefonica.es
  created:        2006-01-18T12:39:59Z
  last-modified:  2014-04-23T17:24:44Z
  source:         RIPE # Filtered

  % Information related to '81.46.0.0/16AS3352'

  route:          81.46.0.0/16
  descr:          RIMA (Red IP Multi Acceso)
  origin:         AS3352
  mnt-by:         MAINT-AS3352
  created:        2002-03-26T11:55:32Z
  last-modified:  2009-08-19T06:59:19Z
  source:         RIPE

  % This query was served by the RIPE Database Query Service version 1.90 (HEREFORD)
  ~~~

### Dnstracer:

- Mostra la cadena de DNS seguida per obtenir informació DNS.

  ~~~
  $ dnstracer www.gogole.es
  Tracing to www.gogole.es[a] via 127.0.0.53, maximum of 3 retries
  127.0.0.53 (127.0.0.53)
  ~~~
---

## 6. Configurar i comprovar interfícies

### Ifconfig (linux), ipconfig (win)

- Obtenir informació sobre les interfícies de xarxa.
- Canviar la configuració d’interfícies de xarxa.
- En windows permet renovar o alliberar adreça IP obtinguada per DHCP.

- Mostrar configuració de les targetes de xarxa

  ~~~
  ifconfig [interficie]
  ~~~

- Configurar interfície

  ~~~
  ifconfig enp3s0 192.168.0.22 netmask 255.255.255.0
  ~~~

- Configurar SUB interfície, així tenim altres adreces a la mateixa interfície

  ~~~
  ifconfig enp3s0:etiqueta 192.168.0.22 netmask 255.255.255.0
  ~~~

- mostrar configuració de les targetes de xarxa (amb /all mostra informació de DNS i DHCP)

  ~~~
  c:> ipconfig

  Configuración IP de Windows

  Adaptador de Ethernet Conexión de red Bluetooth:

     Estado de los medios. . . . . . . . . . . : medios desconectados
     Sufijo DNS específico para la conexión. . :

  Adaptador de Ethernet Ethernet:

     Sufijo DNS específico para la conexión. . :
     Vínculo: dirección IPv6 local. . . : fe80::28be:a3cf:e342:4bca%4
     Dirección IPv4. . . . . . . . . . . . . . : 192.168.1.134
     Máscara de subred . . . . . . . . . . . . : 255.255.255.0
     Puerta de enlace predeterminada . . . . . : 192.168.1.1

  Adaptador de LAN inalámbrica Wi-Fi:

     Estado de los medios. . . . . . . . . . . : medios desconectados
     Sufijo DNS específico para la conexión. . :
  ~~~

- Informació completa

  ~~~
  C:\>ipconfig /all

  Configuración IP de Windows

     Nombre de host. . . . . . . . . : win8
     Sufijo DNS principal  . . . . . :
     Tipo de nodo. . . . . . . . . . : híbrido
     Enrutamiento IP habilitado. . . : no
     Proxy WINS habilitado . . . . . : no

  Adaptador de Ethernet Conexión de red Bluetooth:

     Estado de los medios. . . . . . . . . . . : medios desconectados
     Sufijo DNS específico para la conexión. . :
     Descripción . . . . . . . . . . . . . . . : Dispositivo Bluetooth (Red de áre
  a personal)
     Dirección física. . . . . . . . . . . . . : BC-77-37-2A-73-CA
     DHCP habilitado . . . . . . . . . . . . . : sí
     Configuración automática habilitada . . . : sí

  Adaptador de Ethernet Ethernet:

     Sufijo DNS específico para la conexión. . :
     Descripción . . . . . . . . . . . . . . . : Controladora Gigabit Ethernet Qua
  lcomm Atheros AR8151 PCI-E (NDIS 6.30)
     Dirección física. . . . . . . . . . . . . : 14-DA-E9-62-5F-7C
     DHCP habilitado . . . . . . . . . . . . . : no
     Configuración automática habilitada . . . : sí
     Vínculo: dirección IPv6 local. . . : fe80::28be:a3cf:e342:4bca%4(Preferido)
     Dirección IPv4. . . . . . . . . . . . . . : 192.168.1.134(Preferido)
     Máscara de subred . . . . . . . . . . . . : 255.255.255.0
     Puerta de enlace predeterminada . . . . . : 192.168.1.1
     IAID DHCPv6 . . . . . . . . . . . . . . . : 68475625
     DUID de cliente DHCPv6. . . . . . . . . . : 00-01-00-01-1C-9B-AD-22-14-DA-E9-
  62-5F-7C
     Servidores DNS. . . . . . . . . . . . . . : fe80::1%4
                                         8.8.8.8
                                         8.8.4.4
     NetBIOS sobre TCP/IP. . . . . . . . . . . : habilitado

  Adaptador de LAN inalámbrica Wi-Fi:

     Estado de los medios. . . . . . . . . . . : medios desconectados
     Sufijo DNS específico para la conexión. . :
     Descripción . . . . . . . . . . . . . . . : Intel(R) Centrino(R) Wireless-N 1030
     Dirección física. . . . . . . . . . . . . : BC-77-37-2A-73-C6
     DHCP habilitado . . . . . . . . . . . . . : sí
     Configuración automática habilitada . . . : sí
  ~~~

- Alliberar adreça IP obtinguda per DHCP

  ~~~
  c:> ipconfig /release
  ~~~

- Renovar adreça IP obtinguda per DHCP

  ~~~
  c:> ipconfig /renew
  ~~~

- Esborrar caché DNS de l’equip

  ~~~
  c:> ipconfig /flushdns
  ~~~

- Mostrar caché DNS de l’equip

  ~~~
  c:> ipconfig /displaydns
  ~~~

## iwconfig:
- Equivalent a ifconfig però per targetes wifi.

---

## 7. Configurar i veure taula de rutes

### Route

- Mostrar la taula de rutes d’un host.
- Afegir o treure rutes de la taula de rutes (route add).
- Saber quin és el gateway per defecte.

  ~~~
  $ route
  Kernel IP routing table
  Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
  default         172.16.9.254    0.0.0.0         UG    100    0        0 enp3s0
  link-local      *               255.255.0.0     U     1000   0        0 enp3s0
  172.16.9.0      *               255.255.255.0   U     100    0        0 enp3s0
  192.168.0.0     *               255.255.255.0   U     0      0        0 enp3s0
  192.168.122.0   *            255.255.255.0   U     0      0        0 virbr0
  ~~~

## 8. Arp

- Mostrar taula ARP del host
- Afegir/treure entrades de la taula ARP del host

  ~~~
  $ arp
  Address                  HWtype  HWaddress           Flags Mask            Iface
  172.16.9.2               ether   08:60:6e:82:f3:ea   C                     enp3s0
  172.16.9.5               ether   08:60:6e:82:f8:91   C                     enp3s0
  172.16.9.21              ether   08:60:6e:82:68:fa   C                     enp3s0
  172.16.9.4               ether   08:60:6e:82:f8:ba   C                     enp3s0
  172.16.9.104             ether   14:da:e9:02:9d:1d   C                     enp3s0
  172.16.9.23              ether   08:60:6e:82:ef:5c   C                     enp3s0
  172.16.9.6               ether   30:85:a9:8d:c2:e5   C                     enp3s0
  172.16.9.254             ether   00:0d:88:cd:77:8d   C                     enp3s0
  ~~~

- Esborrar entrada
  ~~~
  $ arp -i enp3s0 -d 172.16.9.2
  ~~~

- Afegir entrada estàtica
  ~~~
  $ arp -s 172.16.9.79 aa:aa:aa:cd:77:8d
  ~~~

- Afegir entrades estàtiques des de fitxer (si no es posa per defecte és /etc/ethers)
  ~~~
  $ arp -f /etc/ethers
  ~~~

## 9. Paquet iproute2 i la comanda ip (linux)

- La gestió de xarxa feta amb ifconfig, route i arp  es pot fer ara amb el paquet iproute2. La comanda més important és ip, amb una sèrie de subcomandes:

- ip address o ip addr: configura les interfícies de xarxa amb IPv4 or IPv6 (subtitueix la comanda  ifconfig)
- ip route: configura l’enrutament (substitueix la comanda route)
- ip rule: configura la política d’enrutament (enrutament avançat en linux)
- ip neigh: gestiona la taula ARP (substitueix la comanda arp)

| Descripció | Antiga comanda | Nova comanda
|-------|-----------|-------|
| Mostrar configuració  | ifconfig [interficie] | ip addr list [interficie] o ip address show [interfície]
|Configurar subinterfície, afegeix una adreça IP.  Es pot fer sense label  |  ifconfig enp3s0:1 192.168.0.22 netmask 255.255.255.0  |  ip address add 192.168.1.2/24 dev eth0 label enp3s0:1 ||
| Mostrar informació de capa 2  |  ifconfig  | ip link show |
| Configurar interfície (ip, mask)  | ifconfig eth2 10.0.0.100 netmask 255.0.0.0  | ip addr add 10.0.0.100/24 broadcast 10.0.0.255 dev eth2 |
| Esborrar configuració interfície | ifconfig eth2 del 10.0.0.2/24   | ip addr del 10.0.0.100/24 dev eth2 |
| Mostrar taula de rutes |route | ip route show|
| Configurar gateway per defecte (si posem del en lloc de add, esborrem  |  route add default gw 192.168.1.254 | ip route add default via 192.168.1.254  |
|  Afegir nova ruta a la taula de rutes (si posem del en lloc de add, esborrem) | route add -net 192.168.200.0 netmask 255.255.255.0 gw 192.168.1.1 | ip route add 192.168.200.0/24 via 192.168.1.1 |
| Veure caché ARP | arp -a | ip neigh show |
| Posar una entrada estàtica a la taula ARP (hw_addr és l’adreça MAC) | arp -s adreçaip hw_addr | ip neigh add adreçaip hw_addr |


 http://rm-rf.es/como-usar-el-comando-ip-en-linux-ejemplos-vs-ifconfig/

## 10. Configuració manual i persistent de xarxa

- Referència: https://netplan.io/examples/

- Per a configurar a mà la xarxa cal editar el fitxer /etc/netplan/01-network-manager-all.yaml. Com veiem, per defecte es gestiona en Ubuntu Desktop pel gestor gafic "NetworkManager".

~~~
// Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: NetworkManager
~~~

- En canvi a Ubuntu server tenim que per defecte netplan gestiona la xarxa:

~~~
// This is the network config written by 'subiquity'
network:
  ethernets:
    enp0s3:
      dhcp4: true
  version: 2
~~~

- Si volem els paràmetres de xarxa estàtics:
~~~
network:
  version: 2
  renderer: networkd
  ethernets:
    enp3s0:
      addresses:
        - 10.10.10.2/24
      gateway4: 10.10.10.1
      nameservers:
          search: [mydomain, otherdomain]
          addresses: [10.10.10.1, 1.1.1.1]
~~~

- Per aplicar els canvis no cal reiniciar, simplement executar:
~~~
$ sudo netplan apply
~~~

- Podem veure l'efecte amb "ip a"

>> MOLT IMPORTANT: Tot i que tinguem múltiples interfícies, només hem de posar en una els paràmetres de gateway per defecte i servidor DNS. Per tant, si una de les interfícies es configura automàticament per DHCP i obté gateway i DNS, NO poseu els paràmetres a les interfćies configurades estàticament. La raó és que la configuració estàtica preval sobre l'obtinguda per DHCP.

---

## 11. Buscar a la xarxa

### Nmap

- Serveix per a escanejar quines màquines hi ha a una xarxa i quins ports té oberts.
- A priori serveix per a fer un inventari de la xarxa, saber quins hosts estan encesos i quins serveis tenen disponibles.
- A més te altres funcions, com esbrinar quin sistema operatiu corre a un determinat host
- També saber quina és la versió d’un determinat servei.

- Escanejar xarxa sencera, només ping

~~~
nmap -v -sn -192.168.0.0/24
Starting Nmap 6.47 ( http://nmap.org ) at 2016-09-18 19:38 Hora de verano romanc
e
Initiating ARP Ping Scan at 19:38
Scanning 254 hosts [1 port/host]
Completed ARP Ping Scan at 19:38, 5.30s elapsed (254 total hosts)
Initiating Parallel DNS resolution of 254 hosts. at 19:38
Completed Parallel DNS resolution of 254 hosts. at 19:38, 2.71s elapsed
Nmap scan report for 192.168.1.0 [host down]
Nmap scan report for 192.168.1.1
Host is up (0.097s latency).
MAC Address: D4:76:EA:0F:FD:58 (Unknown)
Nmap scan report for 192.168.1.2
Host is up (0.0010s latency).
MAC Address: 38:72:C0:A2:69:4B (Comtrend)
Nmap scan report for 192.168.1.3 [host down]
...
Nmap scan report for 192.168.1.128 [host down]
Nmap scan report for redmi4x-redmi (192.168.1.129)
Host is up (0.027s latency).
MAC Address: EC:D0:9F:18:68:22 (Unknown)
Nmap scan report for 192.168.1.130
Host is up (0.27s latency).
MAC Address: 6C:F3:73:16:26:2F (Samsung Electronics Co.)
Nmap scan report for 192.168.1.132
Host is up (0.14s latency).
MAC Address: 8C:BE:BE:2D:8E:7A (Xiaomi Technology Co.)
Nmap scan report for redmi4x-redmi (192.168.1.133)
Host is up (0.043s latency).
MAC Address: EC:D0:9F:18:07:9C (Unknown)
Nmap scan report for 192.168.1.135
Host is up (0.026s latency).
MAC Address: 8C:BE:BE:07:CB:A0 (Xiaomi Technology Co.)
Nmap scan report for 192.168.1.139
Host is up (0.14s latency).
MAC Address: F0:A2:25:45:F8:3F (Private)
Nmap scan report for des (192.168.1.140)
Host is up (0.031s latency).
MAC Address: 00:25:AE:0E:B1:16 (Microsoft)
Nmap scan report for 192.168.1.141
Host is up (0.087s latency).
MAC Address: 54:60:09:44:16:90 (Unknown)
Nmap scan report for 192.168.1.170
Host is up (0.062s latency).
MAC Address: 4C:BB:58:EB:92:7E (Chicony Electronics Co.)
Nmap scan report for 192.168.1.191 (192.168.1.191)
Host is up (0.0010s latency).
MAC Address: 00:26:AB:FF:C3:6F (Seiko Epson)
Nmap scan report for 192.168.1.222
Host is up (0.028s latency).
MAC Address: 00:1C:85:0B:C1:4A (Eunicorn)
...
Nmap scan report for 192.168.1.255 [host down]
Initiating Parallel DNS resolution of 1 host. at 19:38
Completed Parallel DNS resolution of 1 host. at 19:38, 0.35s elapsed
Nmap scan report for 192.168.1.131
Host is up.
Initiating Parallel DNS resolution of 1 host. at 19:38
Completed Parallel DNS resolution of 1 host. at 19:38, 0.06s elapsed
Nmap scan report for 192.168.1.134
Host is up.
Read data files from: C:\Program Files (x86)\Total Network Inventory 3\Nmap
Nmap done: 256 IP addresses (15 hosts up) scanned in 12.46 seconds
           Raw packets sent: 499 (13.972KB) | Rcvd: 15 (420B)
~~~

- Escannejar host concret, ensenya ports oberts

~~~
$ nmap 192.168.0.50

Starting Nmap 7.01 ( https://nmap.org ) at 2016-09-18 14:17 CEST
Nmap scan report for 192.168.0.50
Host is up (0.00012s latency).
Not shown: 994 closed ports
PORT    STATE SERVICE
21/tcp  open  ftp
22/tcp  open  ssh
25/tcp  open  smtp
80/tcp  open  http
139/tcp open  netbios-ssn
445/tcp open  microsoft-ds

Nmap done: 1 IP address (1 host up) scanned in 13.06 seconds
~~~

- https://nmap.org/

---

## 12. Altres configuracions de xarxa

### Fitxer /etc/services i /etc/hosts

- Services: Contenen informació sobre els ports associats als diferents serveis estàndar.
	- A linux /etc/services
	- A windows C:\Windows\System32\drivers\etc\services

~~~
$ cat /etc/services
// Network services, Internet style
//
// Note that it is presently the policy of IANA to assign a single well-known
// port number for both TCP and UDP; hence, officially ports have two entries
// even if the protocol doesn't support UDP operations.
//
// Updated from http:// www.iana.org/assignments/port-numbers and other
// sources like http:// www.freebsd.org/cgi/cvsweb.cgi/src/etc/services .
// New ports will be added on request if they have been officially assigned
// by IANA and used in the real-world or are needed by a debian package.
// If you need a huge list of used numbers please install the nmap package.

tcpmux        1/tcp                // TCP port service multiplexer
echo        7/tcp
echo        7/udp
discard        9/tcp        sink null
discard        9/udp        sink null
systat        11/tcp        users
daytime        13/tcp
daytime        13/udp
netstat        15/tcp
qotd        17/tcp        quote
msp        18/tcp                // message send protocol
msp        18/udp
chargen        19/tcp        ttytst source
chargen        19/udp        ttytst source
ftp-data    20/tcp
ftp        21/tcp
fsp        21/udp        fspd
ssh        22/tcp                // SSH Remote Login Protocol
ssh        22/udp
telnet        23/tcp
smtp        25/tcp        mail
time        37/tcp        timserver
time        37/udp        timserver
rlp        39/udp        resource    // resource location
nameserver    42/tcp        name        // IEN 116
whois        43/tcp        nicname
tacacs        49/tcp                // Login Host Protocol (TACACS)
tacacs        49/udp
re-mail-ck    50/tcp                // Remote Mail Checking Protocol
re-mail-ck    50/udp
domain        53/tcp                // Domain Name Server
domain        53/udp
mtp              57/tcp                // deprecated
tacacs-ds    65/tcp                // TACACS-Database Service
tacacs-ds    65/udp
bootps        67/tcp                // BOOTP server
bootps        67/udp
bootpc        68/tcp                // BOOTP client
bootpc        68/udp
tftp        69/udp
gopher        70/tcp                // Internet Gopher
gopher        70/udp
rje              77/tcp        netrjs
finger        79/tcp
http        80/tcp        www        // WorldWideWeb HTTP
http        80/udp                // HyperText Transfer Protocol
…(continua)
~~~

- Hosts: Conté les resolucions de noms locals
	- A linux /etc/hosts
	- A windows C:\Windows\System32\drivers\etc\hosts

~~~
$ cat /etc/hosts
127.0.0.1        localhost
127.0.1.1        linux-pau
192.168.2.209    www.badstore.net
// The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
~~~

## 13. Subnetting

### ipcalc
- Calcula subnetting.

~~~
$ ipcalc 192.168.0.3/22
Address:   192.168.0.3          11000000.10101000.000000 00.00000011
Netmask:   255.255.252.0 = 22   11111111.11111111.111111 00.00000000
Wildcard:  0.0.3.255            00000000.00000000.000000 11.11111111
=>
Network:   192.168.0.0/22       11000000.10101000.000000 00.00000000
HostMin:   192.168.0.1          11000000.10101000.000000 00.00000001
HostMax:   192.168.3.254        11000000.10101000.000000 11.11111110
Broadcast: 192.168.3.255        11000000.10101000.000000 11.11111111
Hosts/Net: 1022                  Class C, Private Internet
~~~

---

# Exercicis

1. Instal·lar en un equip virtual ubuntu server els serveis:
	- sudo apt install openssh-server
	- sudo apt install apache2
	- sudo apt install vsftpd
	- sudo apt install postfix

2. Posar interfície pont a la màquina virtual.
3. Canviar l’adreça de la màquina virtual (posar 192.168.x.20 on x=Nº de PC de l’aula)
4. Visualitzar la configuració (ifconfig / ip addr).
5. Afegir sub-interfície a la màquina real per a poder veure la màquina virtual (ifconfig / ip addr add).
6. Des de la màquina física, comprova la connectivitat amb la màquina virtual (ping).
7. Fes un escaneig dels ports de la màquina virtual des de la màquina física (nmap).
8. Comprova amb el “client universal” telnet l’accés a aquests serveis. Enganxa el banner de cada servei.
9. Utilitza un client apropiat per a cada servei per comprovar el funcionament dels serveis (ssh, navegador web, ftp).
10. Obté informació sobre un host d’internet anomenat scanme.nmap.org i testeja els serveis que dona (telnet i programes client). Usa nmap. ATENCIÓ: No utilitzar nmap contra d'altres equips per que tenim perill que ens bloquin l'adreça IP pública de l'institut. Això faria que ningú pogués accedir als equips atacats des de l'institut.
11. Esbrina en quins servidors s’allotja el servidor epson.com fent servir nslookup i whois. Quin correu electrònic existeix per a poder denunciar abús?
