# Comandes del sistema d'arxius de Linux

- Tot seguit teniu uns exercicis per repassar les comandes per treballar amb fitxers a Linux

* Obre una consola en mode text picant CTRL+ALT+F1 (per altres consoles, canviar F1 per F2, F3, F4,...) o bé obre una finestra de terminal (CTRL+ALT+T). Podem iniciar sessió amb diferents usuaris per veure com es comporta el sistema amb cadascun d'ells.

* Teniu referències de comandes Linux: Guia d'ubuntu: http://www.guia-ubuntu.com/?title=Comandos

1. Amb el fitxer /etc/passwd:
    - Visualitza'l per pàgines. Passa línia a línia. Pots tornar enrera?
~~~
more /etc/passwd
Barra per a pàgina
Enter per línia a línia
~~~
    - Visualitza el fitxer de manera que es pugui anar amunt i avall. Cerca la paraula var totes les vegades que aparegui. Fes servir l’ajuda de la comanda (posant h).
~~~
    less /etc/passwd
    Cursors amunt i avall per pujar i baixar
    Pàgina amunt i avall
~~~

    - Busca la descripció del fitxer /etc/passwd (NO de la comanda passwd) i explica per a que serveix.
~~~
    man passwd (descriu la comanda passwd)
    man 5 passwd (descriu el contingut del fitxer)
    Conté els usauris locals del sistema
~~~


2. Visualitza els fitxers de /bin que el seu nom:
    - Tingui qualsevol caràcter.
~~~
    ls /bin/*
~~~
    - Comenci per la lletra b.
~~~
    ls /bin/*
~~~
    - Contingui la lletra c.
~~~
    ls /bin/*c*
~~~
    - La segona lletra és a.
~~~
    ls /bin/?a*
~~~
    - Comenci per “ch”.
~~~
    ls /bin/ch*
~~~
    - Acabi per “ch”.
~~~
    ls /bin/*ch
~~~
    - Contingui “ch”.
~~~
    ls /bin/*ch*
    /bin/apt-cache               /bin/gst-launch-1.0
    /bin/apt-ftparchive          /bin/gtk-launch
    /bin/arch                    /bin/gtk-update-icon-cache
    /bin/btattach                /bin/hciattach
    /bin/catchsegv               /bin/hp-check
    /bin/cautious-launcher       /bin/im-launch
    /bin/chacl                   /bin/inputattach
    /bin/chage                   /bin/ischroot
    /bin/chardet3                /bin/isdv4-serial-inputattach
    /bin/chardetect3             /bin/lessecho
    /bin/chattr                  /bin/linux-check-removal
    /bin/chcon                   /bin/locale-check
    /bin/check-language-support  /bin/mcheck
    /bin/cheese                  /bin/networkd-dispatcher
    /bin/chfn                    /bin/patch
    /bin/chgrp                   /bin/pathchk
    /bin/chmod                   /bin/pdfattach
    /bin/choom                   /bin/pdfdetach
    /bin/chown                   /bin/pkcheck
    /bin/chrt                    /bin/podchecker
    /bin/chsh                    /bin/rendercheck
    /bin/chvt                    /bin/sbattach
    /bin/dbus-launch             /bin/setarch
    /bin/echo                    /bin/speech-dispatcher
    /bin/enchant-2               /bin/systemd-machine-id-setup
    /bin/enchant-lsmod-2         /bin/touch
    /bin/fc-cache                /bin/ubuntu-core-launcher
    /bin/fc-match                /bin/watch
    /bin/git-upload-archive      /bin/watchgnupg
    /bin/glib-compile-schemas    /bin/which
    /bin/gnome-characters        /bin/xkbwatch
    /bin/grub-script-check
~~~
    - Només tingui dos caràcters
~~~
    ls /bin/??
    /bin/ar  /bin/dc  /bin/ex  /bin/ld  /bin/mt  /bin/od  /bin/sh  /bin/ul
    /bin/as  /bin/dd  /bin/gs  /bin/ln  /bin/mv  /bin/pr  /bin/ss  /bin/uz
    /bin/bc  /bin/df  /bin/hd  /bin/lp  /bin/nc  /bin/ps  /bin/su  /bin/vi
    /bin/cc  /bin/du  /bin/id  /bin/ls  /bin/nl  /bin/rm  /bin/tr  /bin/wc
    /bin/cp  /bin/ed  /bin/ip  /bin/lz  /bin/nm  /bin/sg  /bin/ua  /bin/xz
~~~

    - Comenci per g i acabi per p.
~~~
    ls /bin/g*p
    /bin/gcov-dump   /bin/gpg-zip  /bin/grub-kbdcomp  /bin/gunzip
    /bin/gnome-help  /bin/grep     /bin/gslp          /bin/gzip
~~~

    - Comenci per g i acabi per p, tenint entre mig la lletra z.

~~~
ls /bin/g*z*p
/bin/gpg-zip  /bin/gunzip  /bin/gzip
~~~

3. Treballem amb directoris i arxius, en mode comandes.
    - Crea dintre el teu directori personal l'estructura de directoris: uno (dintre uno fes groucho, chico i harpo) i dos (dintre posa gaby, miliky, fofito i milikito). Dintre miliky posa els fitxers como, estan i ustedes. I dintre de cada directori crea un fitxer amb el mateix nom que el directori però amb la lletra f davant i amb un text a dintre qualsevol.
~~~
    mkdir /home/pau/uno
    mkdir /home/pau/uno/groucho
    mkdir /home/pau/uno/chico
    mkdir /home/pau/uno/harpo
    mkdir /home/pau/dos
    mkdir /home/pau/dos/gaby
    mkdir /home/pau/dos/miliky
    mkdir /home/pau/dos/fofito
    mkdir /home/pau/dos/milikito
    touch /home/pau/dos/miliky/como
    touch /home/pau/dos/miliky/estan
    touch /home/pau/dos/miliky/ustedes
    touch /home/pau/uno/funo
    touch /home/pau/uno/groucho/fgroucho
    touch /home/pau/uno/chico/fchico
    touch /home/pau/uno/harpo/fharpo
    touch /home/pau/dos/fdos
    touch /home/pau/dos/gaby/fgaby
    touch /home/pau/dos/miliky/fmiliky
    touch /home/pau/dos/fofito/ffofito
    touch /home/pau/dos/milikito/fmilikito

    ...
~~~

- Intenta esborrar el directori uno com ho faries normalment, no esborrant tot l'arbre. Quin és el missatge que et dona?
~~~
$ rmdir /home/pau/uno
rmdir: no s’ha pogut eliminar '/home/pau/uno': El directori no és buit
~~~

- Esborra el fitxer fmiliky.
~~~
    rm /home/pau/dos/miliky/fmiliky
    ...
~~~
    - Visualitza l'estructura de directoris creada.
~~~
$ tree uno
uno
├── chico
│   └── fchico
├── funo
├── groucho
│   └── fgroucho
└── harpo
    └── fharpo
3 directories, 4 files
~~~
~~~
$ tree dos
dos
├── fdos
├── fofito
│   └── ffofito
├── gaby
│   └── fgaby
├── milikito
│   └── fmilikito
└── miliky
    ├── como
    ├── estan
    └── ustedes
4 directories, 8 files
~~~

    - Còpia el tros d'arbre que penja del directori uno dintre el directori milikito amb una sola comanda si pot ser. Visualitza de nou l'estructura de directoris amb que treballem.
~~~
$ cp -R /home/pau/uno /home/pau/dos/milikito
$ tree dos
dos
├── fdos
├── fofito
│   └── ffofito
├── gaby
│   └── fgaby
├── milikito
│   ├── fmilikito
│   └── uno
│       ├── chico
│       │   └── fchico
│       ├── funo
│       ├── groucho
│       │   └── fgroucho
│       └── harpo
│           └── fharpo
└── miliky
    ├── como
    ├── estan
    ├── fmiliky
    └── ustedes
~~~

    - Còpia els arxius que hi ha dintre groucho i chico a harpo amb una sola comanda.
~~~
    $ cp /home/pau/uno/groucho/* /home/pau/uno/chico/* /home/pau/uno/harpo  
    $ tree uno
    uno
    ├── chico
    │   └── fchico
    ├── funo
    ├── groucho
    │   └── fgroucho
    └── harpo
        ├── fchico
        ├── fgroucho
        └── fharpo

    3 directories, 6 files
~~~

    - Mou tots els arxius que hi hagi a gaby, miliky i fofito al directori milikito amb una sola comanda.
~~~
$ mv /home/pau/dos/gaby/* /home/pau/dos/miliky/* /home/pau/dos/fofito/* /home/pau/dos/milikito
$ tree dos
dos
├── fdos
├── fofito
├── gaby
├── milikito
│   ├── como
│   ├── estan
│   ├── ffofito
│   ├── fgaby
│   ├── fmilikito
│   ├── fmiliky
│   ├── uno
│   │   ├── chico
│   │   │   └── fchico
│   │   ├── funo
│   │   ├── groucho
│   │   │   └── fgroucho
│   │   └── harpo
│   │       └── fharpo
│   └── ustedes
└── miliky
~~~

    - Esborra tota l'estructura de directoris i fitxers creats abans.
~~~
$ rm -R /home/pau/uno /home/pau/dos
~~~

4. Treballem amb usuaris i grups:
    - Crea quatre usuaris. Un es diu pep, una altre joan, el tercer pau i per últim angel. Cal que tinguin directori personal. Posa contrasenya a tots per que puguin iniciar sessió. Omple les dades dels usuaris com ara nom i cognoms reals, telèfon, etc. Fes-ho des de consola de comandes.
    ~~~
    sudo adduser pep
    sudo adduser joan
    sudo adduser pau
    sudo adduser angel
    ~~~

    - Fes que pep i joan pertanyin al grup “amics”, que pau pertanyi a “enemics” i que angel pertanyi a tots dos grups. Si no existeixen els grups, crea'ls abans.
~~~
// Hi ha dos tipus de grups: principal i secundari. Només es pot tenir un grup principal i tots els grups secundaris que es vulgui.
// El grup principal és aquell que es posa quan un usuari crea un fitxer
// Quan afegim un usuari a un grup, hem de tenir en compte que -G afegeix a un grup però esborrar tots els altres grups secundaris. Si volem afegir sense esborrar els altres s'ha d'afegir el paràmetre -a (append)
$ sudo addgroup amics
$ sudo addgroup enemics
$ sudo usermod pep -a -G amics
$ sudo usermod joan -a -G amics
$ groups pep
pep : pep amics
$ groups joan
joan : joan amics
$ sudo usermod angel -a -G amics,enemics
~~~

    - Inicia sessions diferents (CTRL+ALT+Fx) amb els usuaris creats per veure que poden fer-ho.
~~~
CTRL+ALT+F4
CTRL+ALT+F5
CTRL+ALT+F6
// Podem iniciar sessió en un terminal en mode gràfic fet login
$ sudo login angel
Contrasenya:
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-52-generic x86_64)
~~~
    - Llista els grups als que pertany l'usuari angel i comprova que pertany a tots dos grups.
~~~
$ groups angel
angel : angel amics enemics
~~~

5.  Treballem amb els drets d'usuari sobre arxius:
    - Qui és el propietari del fitxer /etc/passwd? I el grup? Quin tamany té? Quina és la llista de drets?

~~~
    $ ls -l /etc/passwd
    -rw-r--r-- 1 root root 3150 de nov.   3 13:37 /etc/passwd
    Propietari: root
    Grup: root
    drets:
    propietar: rw- (lectura i escriptura)
    grup: r-- (lectura)
    altres: r-- (lectura)
~~~

  - Fes que pep crei un directori “transfer” que pugui accedir un usuari que pertanyi al grup “amics” (sense poder esborrar res del directori) i que contingui un fitxer “prova.txt” que els amics puguin veure però no canviar.
~~~
$ sudo login pep
pep@equip:~$ mkdir transfer
pep@equip:~$ chown :amics transfer/
pep@equip:~$ chmod g=rx,o= transfer/
pep@equip:~$ ls -la
total 28
drwxr-xr-x 4 pep  pep   4096 de nov.   4 09:01 .
drwxr-xr-x 7 root root  4096 de nov.   3 13:37 ..
-rw-r--r-- 1 pep  pep    220 de nov.   3 13:36 .bash_logout
-rw-r--r-- 1 pep  pep   3771 de nov.   3 13:36 .bashrc
drwx------ 2 pep  pep   4096 de nov.   4 09:00 .cache
-rw-r--r-- 1 pep  pep    807 de nov.   3 13:36 .profile
drwxr-x--- 2 pep  amics 4096 de nov.   4 09:03 transfer
pep@equip:~$ cd transfer/
pep@equip:~/transfer$ echo Hola > prova.txt
pep@equip:~/transfer$ ls -la
total 12
drwxr--r-x 2 pep amics 4096 de nov.   4 09:03 .
drwxr-xr-x 4 pep pep   4096 de nov.   4 09:01 ..
-rw-rw-r-- 1 pep pep      5 de nov.   4 09:03 prova.txt
pep@equip:~/transfer$ cat prova.txt
Hola
pep@equip:~/transfer$ chown :amics prova.txt
pep@equip:~/transfer$ chmod g=r,o= prova.txt
pep@equip:~/transfer$ ls -la
total 12
drwxr--r-x 2 pep amics 4096 de nov.   4 09:03 .
drwxr-xr-x 4 pep pep   4096 de nov.   4 09:01 ..
-rw-r----- 1 pep amics    5 de nov.   4 09:03 prova.txt
~~~

- Comprova amb angel i joan que es pot accedir i veure el contingut de prova.txt però no canviar-lo. Comprova amb pep que ell si pot canviar-lo (indica per què). Comprova amb pau que no hi pot accedir.
    ~~~
    pau@equip:~$ sudo login angel
    Contrasenya:
    Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-52-generic x86_64)
    angel@equip:~$ cd ..
    angel@equip:/home$ cd pep/
    angel@equip:/home/pep$ ls
    transfer
    angel@equip:/home/pep$ cd transfer/
    angel@equip:/home/pep/transfer$ ls
    prova.txt
    angel@equip:/home/pep/transfer$ cat prova.txt
    Hola
    angel@equip:/home/pep/transfer$ echo Adios >> prova.txt
    -bash: prova.txt: S’ha denegat el permís

    $ sudo login pau
    Contrasenya:
    Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-52-generic x86_64)
    pau@equip:~$ cd ..
    pau@equip:/home$ cd pep
    pau@equip:/home/pep$ ls
    transfer
    pau@equip:/home/pep$ cd transfer/
    -bash: cd: transfer/: S’ha denegat el permís
~~~

    - Intenta que joan crei un fitxer dintre el directori transfer o esborrar el fitxer prova.txt. Posa dret d'escriptura al fitxer prova.txt pel grup amics. Pot joan esborrar el fitxer prova.txt? Si no, explica per què i arregla-ho.

~~~
$ sudo login joan
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-52-generic x86_64)
joan@equip:~$ cd ..
joan@equip:/home$ cd pep
joan@equip:/home/pep$ cd transfer/
joan@equip:/home/pep/transfer$ ls -la
total 12
drwxr-x--- 2 pep amics 4096 de nov.   4 09:03 .
drwxr-xr-x 4 pep pep   4096 de nov.   4 09:10 ..
-rw-r----- 1 pep amics    5 de nov.   4 09:03 prova.txt
joan@equip:/home/pep/transfer$ rm prova.txt
rm: voleu eliminar el fitxer ordinari protegit contra escriptura 'prova.txt'? s
rm: no s’ha pogut eliminar 'prova.txt': S’ha denegat el permís
joan@equip:/home/pep/transfer$ cat Hola > fitxer_joan.txt
-bash: fitxer_joan.txt: S’ha denegat el permís

// En una altra consola
pep@equip:~/transfer$ chmod g=rw prova.txt


// Ara tornem a la consola de joan i provem
joan@equip:/home/pep/transfer$ ls -la
total 12
drwxr-x--- 2 pep amics 4096 de nov.   4 09:03 .
drwxr-xr-x 4 pep pep   4096 de nov.   4 09:10 ..
-rw-rw---- 1 pep amics    5 de nov.   4 09:03 prova.txt
joan@equip:/home/pep/transfer$ rm prova.txt
rm: no s’ha pogut eliminar 'prova.txt': S’ha denegat el permís
joan@equip:/home/pep/transfer$ echo Hola > fitxer_joan.txt
-bash: fitxer_joan.txt: S’ha denegat el permís

// Joan no pot esborrar ni crear arxius al directori transfer. Cal donar dret d'escriptura sobre el directori pare (transfer). A la consola de pep
pep@equip:~$ chmod g=rwx transfer/
pep@equip:~$ ls -l
total 4
drwxrwx--- 2 pep amics 4096 de nov.   4 09:03 transfer

// Ara des de la consola de joan
joan@equip:/home/pep/transfer$ echo Hola > fitxer_joan.txt
joan@equip:/home/pep/transfer$ rm prova.txt
joan@equip:/home/pep/transfer$ ls
fitxer_joan.txt
~~~

- Canvia el propietari de l'arxiu prova.txt i posa-li pau. Ara pau el podrà veure? per què? Proposa una manera de fer que pau pugui veure l'arxiu si no podia fer-ho.

~~~
// pep no pot canviar el propietari del seu arxiu.
pep@equip:~/transfer$ chown pau:amics prova.txt
chown: s’està canviant el propietari de 'prova.txt': L’operació no és permesa

//  Ho ha de fer root (en una altra consola iniciem sessió)
$ sudo su
root@equip:/root# cd /home/pep
root@equip:/home/pep# cd transfer/
root@equip:/home/pep/transfer# ls
fitxer_joan.txt  prova.txt
root@equip:/home/pep/transfer# ls -l
total 8
-rw-rw-r-- 1 joan joan 5 de nov.   4 09:25 fitxer_joan.txt
-rw-rw-r-- 1 pep  pep  5 de nov.   4 09:29 prova.txt
root@equip:/home/pep/transfer# chown pau prova.txt
root@equip:/home/pep/transfer# ls -l
total 8
-rw-rw-r-- 1 joan joan 5 de nov.   4 09:25 fitxer_joan.txt
-rw-rw-r-- 1 pau  pep  5 de nov.   4 09:29 prova.txt

// Des d'una altra consola inciem sessió per a pau
pau@equip:~$ cd /home/pep
pau@equip:/home/pep$ ls
transfer
pau@equip:/home/pep$ cd transfer/
bash: cd: transfer/: S’ha denegat el permís

// Pau no pot entrar al directori per que no té dret de pas i no pertany al grup amics. Podem afergir pau al grup d'amics o també donar dret de pas a altres (més perillós)
root@pau-W65-W67RB:/home/pep/transfer# usermod pau -a -G amics
root@equip:/home/pep/transfer# groups pau
pau : pau amics
~~~

- Crea un directori "transfer" al teu directori personal on tothom pugui veure i afegir coses, inclús albert que acabes de crear ara i no pertany a cap grup d'amics i enemics.
~~~
// Cal crear dir i donar drets rwx al grup others (altres)
$ mkdir transfer
$ chown o=rwx transfer
~~~

Documents a consultar:
- man, comanda --help

Apunts de classe

Comandes i fitxers utilitzats:

- login
- touch
- rm
- mv
- cp
- ls
- vi
- mkdir
- rmdir
- cd
- tree
- useradd
- userdel
- groupadd
- groupdel
- groups
- usermod
- chmod
- chown
- more
- less
- man

alguna més...
