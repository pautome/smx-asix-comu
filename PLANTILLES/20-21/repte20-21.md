# REPTES:

**ATENCIÓ**: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre nom+cognom per a que a les captures es pugui veure.

Convertiu el fitxer **Markdown en pdf** abans de lliurar la pràctica i lliureu aquest. A més, afegiu també la carpeta d'imatges amb el document .md, en un fitxer .zip

Teniu l'exercici a fer al final d'aquest enunciat. Llegiu primer tot el document abans de començar la pràctica.

Abans de començar **podeu fer un "snapshot" o instantània de la maquina virtual**. Això farà que l'estat actual de la màquina virtual es guardi i pugueu fer tots els canvis que vulgueu sense por de fer malbé alguna cosa. Poseu-li de nom "Abans de (l'acció que aneu a fer)...". En acabar, haureu d'esborrar aquesta instantània per tornar a l'estat anterior.

En general, heu de **comprovar SEMPRE que els canvis de configuració que feu produeixen l'efecte desitjat** amb el conjunt de proves necessari.

---

## Objectius

- L'objectiu d'aquesta pràctica és ...

---

## Referències i ajuda

---

## Introducció (teoria)

---

## Enunciat reptes
