# REPTE:

**_MOLT IPORTANT:_ CAL LLEGIR AMB ATENCIÓ A CADA ENUNCIAT**

- Poseu el **nom i cognoms al document i copieu l'enunciat**. Poseu les preguntes ressaltades i la resposta a sota.

- **ATENCIÓ**: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, carpetes, etc. han de ser el vostre nom+cognom. **A TOTES les captures s'ha de veure el vostre nom i cognoms**. Si no es fa així, caldrà tornar a repetir TOTA la pràctica amb les captures adients.

- Cada equip ha de tenir un nom de host diferent (firewall, server-web, etc.). Això facilitarà saber en quin equip estem quan ens hi connectem amb una consola remota.

- **Responeu a totes les preguntes**, si cal cercant informació als apunts del mòdul, als manuals de les aplicacions, fitxers d'ajuda i "man" i si no es troba la solució, fins i tot a Internet com a últim recurs.

- A les captures heu de **MARCAR amb cercles o fletxes la part rellevant**. Aquestes captures han de tenir un context, o sigui, saber d'on surten. No feu mini-captures ja que no sabré d'on surt la informació. Podeu fer servir "FlameShot" per capturar i afegir fletxes, etc.

- Abans de començar **podeu fer un "snapshot" o instantània de la maquina virtual**. Això farà que l'estat actual de la màquina virtual es guardi i pugueu fer tots els canvis que vulgueu sense por de fer malbé alguna cosa. Poseu-li de nom "Abans de (l'acció que aneu a fer)...". En acabar, haureu de revertir aquesta instantània per tornar a l'estat anterior.

- En general, heu de **comprovar SEMPRE que els canvis de configuració que feu produeixen l'efecte desitjat** amb el conjunt de proves necessari.

---

**ATENCIÓ: La informació proporcionada té un caràcter educatiu i en cap cas se'n pot fer un ús delictiu o maliciós. Cal fer un ús responsable i ètic dels coneixements adquirits. MAI ho heu de fer amb un equip en producció (màquina física o de la feina). En cas contrari, es pot incórrer en delictes tipificats al codi Penal amb penes que poden suposar multes i fins i tot presó.**

**Es declina tota responsabilitat en el cas que aquesta informació sigui utilitzada amb finalitats il·lícites o delictives. També es declina tota responsabilitat en cas de destrucció total o parcial de dades. Cal fer abans còpies de seguretat per no perdre cap dada.**

---

## Programari de Microsoft

- Win 7, 8.1 i 10 ova  (IEUser, Passw0rd!) - https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/
- ISO o disc dur virtual (VHD) de Windows Server - https://www.microsoft.com/es-es/evalcenter/evaluate-windows-server-2012-r2

---

## Objectius

- L'objectiu

---

## Referències i ajuda

- Eines a fer servir
- Enllaços d'ajuda
- Fitxers i material

---

## Introducció (teoria)

---

## Enunciat repte
