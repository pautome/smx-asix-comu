# Missatge email inicial

Bon dia

Us envio les informacions referents al mòdul.

- El curs de Moodle el podeu accedir aquí: https://moodle.iescarlesvallbona.cat/course/view.php?id=282
- Heu de posar una foto de la vostra cara, de front, al perfil de Moodle.
- La clau d'inscripció al Moodle M11 és
Grup A: M11A
Grup B: M11B
- Per una altra banda, heu de crear un compte a Gitlab.com amb el correu electrònic del centre: nom@iescarlesvallbona.cat.

- Els apunts i els enunciats dels exercicis del mòdul els podeu accedir a l'adreça: https://gitlab.com/pautome/m11-asix-pub

- Heu de crear un compte de Discord EXCLUSIU per les classes a distància.
  - Heu de registrar-vos amb el correu del centre nom@iescarlesvallbona.cat.
  - Heu de posar una foto de la vostra cara, de front, al perfil de Discord.
  - Heu de posar el vostre nom i cognoms reals.
  - podeu afegir-vos a Discord amb aquesta invitació: https://discord.gg/8bJuRSS

Salutacions i bon inci de curs.
