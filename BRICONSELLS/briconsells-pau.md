# Briconsells (Consells de bricolatge)

## RTFM (sigles molt importants per IT Crew)

- Read The Full Manual (Llegir el manual complet)
- Read The Fucking Manual (Llegir el fotut manual)
- Frase cèlebre: "Ante la duda, miro la AYUDA"

- Ajuda del sistema Linux:
  - man comanda (repassar format del man)
  - man 5 comanda (mirar pàgines de man)
  - info comanda (manual alternatiu)
  - apropos paraula (mostra totes les pàgines del manual que contenen la paraula)

---

## Posar noms d'objectes respectant grafia anglesa

- Els objectes poden ser arxius, carpetes, comptes d'usuaris, objectes de directori actiu (AD), etc.
- Per evitar problemes:
  1. No posar espais als noms. Prefereixo posar - o _ si vull separar paraules.
  2. No posar accents, ni símbols no anglesos com ñ o ç.
  3. Posar noms curts. Fer servir abreviatures.

---

## Llegir els missatges d'error i informatius

- La sortida de les comandes (el missatge que retornen) SEMPRE s'ha de llegir, així com els errors.
- Si una comanda de Linux dona un missatge, normalment és un error o un avís.
- Fer una captura dels errors (copiar el text, capturar pantalla o fer una foto si no es pot fer d'una altra manera)

---

## Registrar la informació rellevant en cas de problemes

- Per entendre-ho, veiem el que passa quan anem al metge (també val per l'advocat, el mecànic, etc.):
  - Doctor, estic malalt.
  - Què li passa? (el metge espera una descripció del problema, símptomes, etc.)
  - Digui-m'ho vostè que és el metge!
  - Si no em diu que li passa, no el puc ajudar.

- En el nostre cas:
  - Senyor tècnic, no em funciona l'ordinador.
  - Què li passa? (el tècnic espera una descripció del problema, símptomes, etc.)
  - Ahir funcionava i jo no l'he tocat!
  - Si no em diu què li passa, que és el que ha fet, quins missatges d'error apareixien, no el puc ajudar.

- Anoto informació rellevant (símptomes, situació, què feia, que havia fet abans, l'últim que he tocat, etc.)

- L'usuari sempre diu: **"no he fet res, ahir funcionava, a casa m'anava"**. No fer el mateix.


---

## Documentar-ho tot

- Anotar tot el procediment.
  - En cas de problemes, anotar els símptomes.
  - Si s'ha trobat la solució, cal afegir-la.

- Així no es perdran tantes hores la propera vegada que es necessiti fer el mateix.
- Fer **còpia dels recursos consultats**. No contentar-se només amb una URL. Així, si el recurs consultat desapareix, tinc còpia pròpia.

---

## Fer les captures adients en cada cas

- **Si és mode gràfic**, capturar pantalla (la part de pantalla rellevant, sempre agafant suficient context que permeti saber què estem fent).
  - Cal senyalar amb un cercle, fletxa, missatge, etc. el que és rellevant de la captura.


- **Si és mode consola**, copiar el text. Em connecto via ssh si és una màquina virtual (així puc copiar text).
  - El text es pot reaprofitar (copy-paste).
  - Puc posar el format que vulgui.
  - El fons NO serà negre i podré imprimir bé.

---

## Posar extensió als arxius (de text i altres)

- El sistema operatiu (**Windows, Linux, etc.**) i el programari reconeix el tipus d'arxiu i obre el programa adequat atenent a l'**extensió**.

- En **Linux** a més, si el fitxer te format binari, a la capçalera hi ha el "numero màgic" que determina el tipus d'arxiu. Així no caldria l'extensió. La comanda "file" indica el tipus d'arxiu.

- Per exemple, **Atom** reconeix que el fitxer és Markdown si li posem extensió .md  

- **ATENCIÓ:** Vigilar per que hi ha arxius que no tenen l'extensió correcta i poden portar a engany (ex: arxiu imatge jpeg que en realitat és png no s'obrirà bé).

---

## Seguir el mètode científic per a resoldre problemes, investigar o implementar algun sistema

Passes a seguir:
1. **Realitzar una pregunta**: per exemple, per què no puc navegar per internet des del meu ordinador?
2. **Fer una investigació de les dades**: per exemple, mirar missatges d'error del navegador web, comprovar amb ping si hi ha connectivitat, etc.
3. **Enunciar una hipòtesi**: per exemple, el servidor DHCP no m'ha donat paràmetres IP correctament.
4. **Fer una prova per descartar o validar la hipòtesi**: per exemple, comprovar amb comandes si tenim o no paràmetres IP correctes.
5. **Documentar els resultats** (tant en cas positiu com negatiu, ja que dels errors també aprenem i així no tornem a provar hipòtesis ja comprovades).
6. Si la hipòtesi no es correcta o parcialment falsa, tornar al pas 2 (o al 3 si no hi ha més dades a obtenir).

<img src="img/briconsells-pau-53cd17cf.png" width="500" align="center" />


---

## Busco informació en fonts fiables i la contrasto (29/10/2019)

- Vaig a llocs web on hi ha informació de primera mà. Per exemple, si busco informació sobre protecció de dades, vaig a les agències de protecció de dades (catalana, espanyola, europea). O si busco informació sobre Apache, vaig a la web oficial d'Apache.
- Si no tinc ni idea d'on trobar la informació, busquem amb buscadors d'internet (existeixen d'altres a part de Google, per exemple Bing, Duck duck go, etc).
- Quan trobi informació que m'interessa, la contrasto amb d'altres fonts d'informació per assegurar-me de la veracitat o que es pot aplicar.

---

## Comprovo la integritat dels fitxers

- Abans de fer servir els fitxers, sobretot els de gran tamany, comprovo la integritat calculant el hash. Normalment hi ha un fitxer de text amb el hash calculat. Calcularem el hash (sha256sum fitxer o md5sum o "funcio-hash-que-toqui"sum) i el resultat el comparem amb el hash que ens han donat.

---

## Referència de símbols en comandes Linux

- **[op]** Implica que op és opcional.
- **op1 | op2** implica que es pot usar op1 o bé op2.
- **(op1 | op2)** Obligatori op1 o bé op2.

#### Paràmetres

- -a és paràmetre d'una sola lletra
- -abc és equivalent a posar -a -b -c
- --opcio és paràmetre escrit en una paraula (llarg)

### Comentaris en el codi

- Alguns símbols que indiquen que la línia és un comentari en un fitxer de codi, script o fitxer de configuració.
  - **//** comentari en C++, per exemple  
  - **;;** comentari en Assembler o fitxer de configuració de bind, per exemple
  - **\#** comentari en Bash, per exemple
  - **/*** comentari */ comentari en C. Permet un comentari de vàries línies
  - **"""** Inici i final de comentari de vàries línies en Python

---

## En la configuració de xarxa cal tenir en compte...

- **MOLT IMPORTANT**: Tot i que hi hagi múltiples interfícies en un host, només s'ha de posar en una d'aquestes els paràmetres de gateway per defecte i servidor DNS.

- Per tant, si una de les interfícies es configura de forma automàtica per DHCP i obté gateway i DNS, NO posar els paràmetres a les interfícies configurades estàticament. La raó és que la configuració estàtica preval sobre l'obtinguda per DHCP.
