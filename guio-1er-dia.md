# Guió dels primers dies de classe

## Preparació

- Actualitzar curs de Moodle.
  - Ocultar lliçons no accessibles.
  - Posar fòrmules de notes.

## Presentació

- Comentar visió general del mòdul. Potser mostrar fitxa d'inici?

## Primer dia

- Llegir fitxa d'inici i comentar mínimament els continguts.
- Moodle:
  - Accedir al Moodle del mòdul i anotar incidències.
  - Posar foto al perfil.
- Gitlab
  - Crear comptes
  - Afegir al grup de l'aula
- Començar a explicar Atom i com fer projectes a Gitlab
- Explicar docuemtnació amb Markdown, bàsic. Posar Exercici de Markdown.

- Teoria primer dia (conceptes seg.)
