# NORMATIVA DE DOCUMENTACIÓ DELS PROJECTES

1. Els terminis de lliurament s’han de complir curosament. El lliurament més tard de la data acordada suposarà una penalització de la nota.

1. Cada alumne ha de portar els dies que es faci projecte:
  - Apunts i material de consulta, com ara documents electrònics, tutorials, etc.
  - Altres materials, com ara cables de xarxa de l'alumne, eines, etc.

## LLIURAMENT DE MEMÒRIES

1. Cada lliurament de projecte vindrà acompanyat d’una memòria explicativa en una carpeta gitlab.

1. Cada document Markdown que feu ha de contenir tota la informació referent a un únic tema (per exemple, configuració de servei DHCP en un fitxer amb explicació del procediment i proves de funcionament). Així amb el titol del document sabrem on buscar la informació.

1. En general, heu de comprovar SEMPRE que els canvis de configuració que feu produeixen l'efecte desitjat amb el conjunt de proves necessari. Per tant, dissenyeu proves per demostrar que el que estem fent funciona, i expliqueu el funcionament d'aquestes proves.

1. Comproveu que tots els canvis funcionen i feu captures de pantalla que ho evidenciïn si fa falta.

1. Documenteu el procés per la vostra referència posterior, i la dels usuaris que dependran de vosaltres.

1. Les pràctiques es faran en format electrònic Markdown.

## MEMÒRIA/MANUAL DEL PROJECTE

1. Per fer la memòria es recomana fer servir el sentit comú. Com que és un projecte del que volem cobrar uns diners, cal que tot estigui explicat amb detall. Recomano que intenteu explicar el procés a algú del vostre entorn o potser penjar-ho a algun blog demanant comentaris (si teniu temps).

1. NO esmenteu per a res les màquines virtuals. A tots els efectes, el projecte és real i es fa en equips reals.

1. Heu de fer el manual per blocs o apartats organitzatius de la manera següent (és una proposta i podeu fer-ho a la vostra, però els continguts esmentats han d'aparèixer a la memòria. De fet, cada apartat pot tenir sub-apartats):

## Exemple d'organització de la documentació

1. Plantejament del projecte.
  1. Resum de les decisions adoptades tenint en compte els condicionats que ha donat l'empresa.
  1. Mapa lògic de la xarxa (amb servidors, dispositius, hosts i dades dels hosts)
  1. Taula de distribució de IP's, noms i adreces MAC.

1. Manual tècnic d'instal·lació, configuració i administració.
  1. Requeriments dels equips de la xarxa.
  1. Configuració i prova bàsica dels serveis i programes de manera local.
  1. Depuració de logs.
  1. Directives dels fitxers de configuració dels serveis, si n'hi ha (què són, explicació i exemples).
  1. Errors freqüents en la configuració i la seva solució. (si escau).

1. Manual d'explotació
  1. Configuració i prova bàsica dels clients.
  1. Comandes i/o descripció de sessions de treball dels clients i/o  les eines de test.
  1. Anomalies detectades i la seva solució.

1. Bibliografia
